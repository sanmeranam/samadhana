export class AddressModel {
    house: string;
    area: string;
    village: string;
    city: string;
    state: string;
    country: string;
}


export class UserModel {
    _id: string;
    first_name: string;
    last_name: string;
    avtar: string = 'assets/user.svg';
    password: string;
    username: string;
    email: string;
    contact: string;
    address: AddressModel;
    description: string;
    role: any;
    active: boolean;
    activity: any[];
    created: Date;
    data: string;
    modified: Date;
    language: string;

    get fullname(): string {
        return this.first_name + " " + this.last_name
    }
}

export enum RoleTypeEnum {
    ADMIN = "ADMIN",
    USER = "USER"
}

export class RoleModel {
    _id: string;
    name: string;
    type: RoleTypeEnum;
    description: string;
    access: any;
    actions: any;
    district: string;
    taluk: string;
    display_name: string;
    icon: string;
}