import { TranslateService } from '@ngx-translate/core';
import { ConfigService } from './../../../services/config.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { smoothlyMenu } from '../../../app.helpers';
import { UserService } from '../../../services/user.service';
import { UserModel } from '../../../models/user.model';
declare var jQuery: any;

@Component({
    selector: 'topnavbar',
    templateUrl: 'topnavbar.template.html'
})
export class TopnavbarComponent implements OnInit {

    private currentUser: UserModel;
    constructor(public config: ConfigService, public userServ: UserService, private route: Router, private tran: TranslateService) {

    }

    ngOnInit() {
        jQuery("#top-nav-close").hide();
        // this.userServ.me().subscribe((u: UserModel) => {
        //     this.currentUser = u;
        //     this.changeLanguage(u.language);
        // })
    }

    changeLanguage(sName) {
        this.tran.use(sName);
        // this.currentUser.language = sName;
        // this.userServ.editUser(this.currentUser);
    }
    getCurrentLang() {
        switch (this.tran.currentLang) {
            case 'en': return "English";
            case 'ka': return "ಕನ್ನಡ";
        }
    }
    toggleNavigation(): void {
        jQuery("body").toggleClass("mini-navbar");
        if (jQuery("body").hasClass("mini-navbar") && !jQuery("body").hasClass("body-small")) {
            jQuery("#top-nav-close").hide();
        } else {
            jQuery("#top-nav-close").show();
        }
        smoothlyMenu();
    }

    logout() {
        this.userServ.logout();
        this.route.navigate(['/login']);
    }

}
