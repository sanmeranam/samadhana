import { ConfigService } from '../../services/config.service';
import { Component, OnInit } from '@angular/core';
import { Globals } from '../../app.global';

@Component({
  selector: 'app-mailbox',
  templateUrl: './mailbox.component.html',
  styleUrls: ['./mailbox.component.css']
})
export class MailboxComponent implements OnInit {

  constructor(private config:ConfigService) { }

  ngOnInit() {
    
  }

}
