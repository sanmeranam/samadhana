var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var User = require('../schemas/User.schema');
var authconfig = require("./auth.config");

// Setup work and export for the JWT passport strategy
module.exports = function (passport) {
    var opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
        secretOrKey: authconfig.secret
    };

    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        User.findById(jwt_payload._id, '-password').populate("role").then((user) => {
            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        }).catch((err) => {
            return done(err, false);
        });
    }));
};
