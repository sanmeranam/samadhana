var Ticket = require("../schemas/Ticket.schema");
var Access = require("../schemas/AccessLog.schema");
var mongoose = require('mongoose');
var NotifManage = require("./notification.controller");

var fnPromiseToResponse = function (oProms, res, hook) {
    oProms.then(function (data) {
        res.json(data);
        if (hook) hook(data)
    }).catch(function (e) {
        res.json(e);
    });
};


module.exports = {
    createTicket: function (req, res) {
        try {


            var oTicket = new Ticket();
            oTicket.eat_date = new Date(Date.now() + (1000 * 60 * 60 * 24 * 30));
            oTicket.history = {
                create: {
                    date: Date.now(),
                    user: req.user._id
                },
                modified: []
            };
            oTicket.stages = {
                current: {
                    user: null,
                    user_group: '',
                    action: '',
                    in_date: Date.now(),
                    eta_date: new Date(Date.now() + (1000 * 60 * 60 * 24 * 5)),
                    out_date: null
                },
                history: []
            }

            oTicket.memo = []

            res.json(oTicket);
            // fnPromiseToResponse(oTicket.save().then(function (data) {
            //     return Ticket.populate(data, [
            //         'history.create.user'
            //     ])
            // }), res);
        } catch (e) {
            console.error(e);
            res.send(e.toString());
        }
    },
    saveNew: function (req, res) {
        fnPromiseToResponse(new Ticket(req.body).save(), res, function (oTicket) {
            NotifManage.dispatchCreationAlert(oTicket);
        });
    },
    discard: function (req, res) {
        var id = req.params.id;
        fnPromiseToResponse(Ticket.findByIdAndRemove(id), res);
    },
    update: function (req, res) {
        var id = req.params.id;
        var oTicket = req.body;

        oTicket.history.modified = oTicket.history.modified || [];
        oTicket.history.modified.push({
            date: Date.now(),
            user: req.user._id,
            comment: "Updated details in ticket id " + oTicket.ticket_num
        });
        fnPromiseToResponse(Ticket.findByIdAndUpdate(id, oTicket), res);
    },
    getTicket: function (req, res) {
        var id = req.params.id;
        fnPromiseToResponse(Ticket.findById(id).then(function (data) {
            return Ticket.populate(data, [
                'history.create.user',
                'history.modified.user',
                'stages.history.user',
                'stages.current.user',
                'memo.user'
            ])
        }), res);
    },
    getAllTicket: function (req, res) {
        fnPromiseToResponse(Ticket.find({}).then(function (data) {
            return Ticket.populate(data, [
                'history.create.user',
                'history.modified.user',
                'stage.history.user',
                'stages.current.user',
                'memo.user'
            ])
        }), res);
    },
    getAllTicketByQuery: function (req, res) {
        fnPromiseToResponse(Ticket.find(req.body).then(function (data) {
            return Ticket.populate(data, [
                'history.create.user',
                'history.modified.user',
                'stage.history.user',
                'stages.current.user',
                'memo.user'
            ])
        }), res);
    },
    actionPerformed: function (req, res) {
        try {
            var id = req.params.id;
            var action = req.params.action;
            var oTicket = req.body;
            var ticketType = oTicket.request_type;
            var oActionConfig = req.flowConfig[ticketType]['ACTIONS_DEFF'][action];
            var revertUser = null;

            oTicket.status = oActionConfig.TO_STATUS;
            oTicket.stages.current.out_date = Date.now();
            oTicket.stages.current.action = action;
            oTicket.stages.current.status = oTicket.status;
            oTicket.stages.current.user = req.user._id;
            oTicket.stages.history = oTicket.stages.history || [];
            oTicket.stages.history.push(oTicket.stages.current);
            if (oActionConfig.REVERT_TO) {
                var his = oTicket.stages.history.filter(function (stage) {
                    return stage.action == oActionConfig.REVERT_TO
                });
                if (his && his.length && his[0].user) {
                    revertUser = his[0].user;
                }
            }

            oTicket.stages.current = {
                user: revertUser,
                user_group: '',
                action: '',
                in_date: Date.now(),
                eta_date: new Date(Date.now() + (1000 * 60 * 60 * 24 * 7)),
                out_date: null
            }

            oTicket.history.modified = oTicket.history.modified || [];
            oTicket.history.modified.push({
                date: Date.now(),
                user: req.user._id,
                comment: "Action performed '" + action + "' and status changed to '" + oActionConfig.TO_STATUS + "'"
            });

            var hasOnDemandUpdate = oTicket.toUser;
            delete (oTicket.toUser);

            fnPromiseToResponse(Ticket.findByIdAndUpdate(id, oTicket), res);

            if (hasOnDemandUpdate) {
                NotifManage.dispatchOnDemandAlert(oTicket);
            } else if (action == "DO_MARK_COMPLETE") {
                NotifManage.dispatchCompleteAlert(oTicket);
            }
        } catch (e) {
            res.send(e.toString());
        }
    },

    ticketPartialUpdate: function (req, res) {
        var id = req.params.id;
        var data = req.body;
        if (data.stage && data.stages.current.user) {
            data.stages.current.user = mongoose.Types.ObjectId(data.stages.current.user);
        }

        fnPromiseToResponse(Ticket.update({ _id: id }, { $set: data }), res);

        Ticket.findById(id).then(function (oTicket) {
            oTicket.history.modified = oTicket.history.modified || [];
            oTicket.history.modified.push({
                date: Date.now(),
                user: req.user._id,
                comment: "Updated '" + JSON.stringify(data) + "' in ticket id " + oTicket.ticket_num
            });
            Ticket.findByIdAndUpdate(oTicket._id, oTicket);
        });
    },
    getBasicCouts: function (req, res) {
        var id = req.user._id;
        var type = req.params.type;
        var all = 0, blank = 0, me = 0;
        var queryAll = { 'request_type': type };
        var queryBlank = { 'stages.current.user': null, 'request_type': type };
        var queryMe = { 'stages.current.user': id, 'request_type': type };

        if (req.body && req.body.district && req.body.taluk) {
            queryMe['property_details.district'] = req.body.district;
            queryMe['property_details.taluk'] = req.body.taluk;

            queryBlank['property_details.district'] = req.body.district;
            queryBlank['property_details.taluk'] = req.body.taluk;
        }

        Ticket.find(queryAll).countDocuments().then(count => {
            all = count;
            return Ticket.find(queryBlank).countDocuments()
        }).then(count => {
            blank = count;
            return Ticket.find(queryMe).countDocuments()
        }).then(count => {
            me = count;
            res.json({
                total: all,
                blank: blank,
                me: me,
                me_id: id
            })
        })
    },
    userSearchText: function (req, res) {
        var body = req.body;
        if (body.ticket_num && body.mobile) {
            Ticket.findOne({ ticket_num: body.ticket_num, 'requestor.contact': body.mobile }).then(data => {
                if (data) {
                    data = data.toJSON();
                    delete (data.history.modified);
                    delete (data.memo);
                    delete (data.priority);
                    delete (data.stages);
                    res.json(data);
                } else {
                    res.json({ error: "Not found" });
                }

            });
        } else {
            res.json({ error: "Not found" });
        }
    },
    getLandingPageData: function (req, res) {
        var dateCount, statusCount, total;
        var allRequest = [];

        allRequest.push(Ticket.aggregate([
            {
                $group: {
                    _id: { $month: "$history.create.date" },
                    count: { $sum: 1 },
                    type: { $addToSet: "MONTH" }
                }
            }
        ]));

        allRequest.push(Ticket.aggregate([
            {
                $group: {
                    _id: "$status",
                    count: { $sum: 1 },
                    type: { $addToSet: "STATUS" }
                }
            }
        ]));

        allRequest.push(Ticket.aggregate([
            {
                $group: {
                    _id: null,
                    count: { $sum: 1 },
                    type: { $addToSet: "TOTAL" }
                }
            }
        ]));

        allRequest.push(Access.aggregate([
            {
                $group: {
                    _id: "$type",
                    count: { $sum: 1 },
                    type: { $addToSet: "ACCESS" }
                }
            }
        ]));

        Promise.all(allRequest).then(function (values) {
            res.json(values);
        });
    }
};