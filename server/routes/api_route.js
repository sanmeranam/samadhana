var express = require('express');
var router = express.Router();


var api_route = {
  _regGet: {},
  _regPost: {},
  registerGet: function (path, fnCallback) {
    this._regGet[path] = fnCallback;
  },
  registerPost: function (path, fnCallback) {
    this._regPost[path] = fnCallback;
  }
};

router.get('/*', function (req, res, next) {
  var path = req.url.split("?")[0];
  path = path.replace("/api", "");
  if (api_route._regGet[path]) {
    api_route._regGet[path](req, res);
  } else {
    next();
  }
});

router.post('/*', function (req, res, next) {
  var path = req.url.split("?")[0];
  path = path.replace("/api", "");
  if (api_route._regPost[path]) {
    api_route._regPost[path](req, res);
  } else {
    next();
  }
});

api_route.router = router;

module.exports = api_route;
