import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { TopnavbarComponent } from "./topnavbar.component";

@NgModule({
    declarations: [TopnavbarComponent],
    imports: [BrowserModule, TranslateModule],
    exports: [TopnavbarComponent],
})

export class TopnavbarModule {
    
}