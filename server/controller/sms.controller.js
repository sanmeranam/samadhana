var request = require('request');
var Settings = require("../schemas/Settings.schema");

module.exports = SMSController = {
    sendSMS: function (to, message, callback, isUnicode) {
        Settings.findOne({ name: "sms_config" })
            .then(function (oResult) {
                var oConfig = oResult.value;
                if (oConfig.method == "POST") {
                    request.post({
                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                        url: oConfig.url,
                        body: SMSController._toSearchURL(oConfig.params, to, message, isUnicode)
                    }, function (error, response, body) {
                        callback(body);
                    });
                } else {
                    request.post({
                        headers: { 'content-type': 'application/x-www-form-urlencoded' },
                        url: oConfig.url + "?" + SMSController._toSearchURL(oConfig.params, to, message, isUnicode)
                    }, function (error, response, body) {
                        callback(body);
                    });
                }
            });
    },
    _toSearchURL: function (aParams, mobile, content, isUnicode) {
        return aParams.map(function (item) {
            if (item.value == "{message}") {
                return item.name + "=" + content;
            }
            if (item.value == "{mobile}") {
                return item.name + "=" + mobile;
            }
            return item.name + "=" + item.value
        }).join("&") + (isUnicode ? "&unicode=true" : "");
    },
    _formatUnicodeSMS: function (content) {
        var st = "", c;
        for (var i = 0; i < content.length; i++) {
            c = content.charCodeAt(i).toString(16).toUpperCase();
            c = c.length < 4 ? '0' + c : c;
            c = c.length < 4 ? '0' + c : c;
            st += c;
        }
        content = '@U' + st;
        return content;
    },
    trialSend: function (req, res) {
        var to = req.params.to;
        var type = req.params.type;
        Settings.find({ name: "sms_template" })
            .then(function (result) {
                result = result[0].value;
                temp = result.filter(function (item) {
                    return item.type == type;
                })[0];

                if (temp.local) {

                    SMSController.sendSMS(to, SMSController._formatUnicodeSMS(temp.content), function (r) {
                        res.json(r);
                    }, true);
                } else {
                    SMSController.sendSMS(to, temp.content, function (r) {
                        res.json(r);
                    }, false);
                }
            })
    }
};