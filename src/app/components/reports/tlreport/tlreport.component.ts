import { TicketService } from './../../../services/ticket.service';
import { UserModel } from './../../../models/user.model';
import { UserService } from './../../../services/user.service';
import { RestService, TABLE_TYPE } from './../../../services/rest.service';
import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { SettingModel } from '../../settings/sms/sms.model';
import { TicketModel } from '../../../models/ticket.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-tlreport',
  templateUrl: './tlreport.component.html',
  styleUrls: ['./tlreport.component.scss']
})
export class TlreportComponent implements OnInit, OnDestroy {


  @Output() nav = new EventEmitter();
  @Input() state: any;

  currentFilter: TicketFitler;
  defaultQuery: SettingModel<TicketFitler>;
  filterList: SettingModel<TicketFitler>[];
  baseFiltrObject: Object;
  ticketList: TicketModel[] = [];
  filterLoading: number = 0;

  constructor(private rest: RestService, private user: UserService, private ticket: TicketService) {
    this.defaultQuery = new SettingModel<TicketFitler>();
    this.defaultQuery.name = "SAVED_FILTER";
    this.defaultQuery.value = new TicketFitler();


    this.rest.getByField(TABLE_TYPE.SETTINGS, "name", "STATUS_TYPES").subscribe((data: SettingModel<string[]>[]) => {
      this.defaultQuery.value.filter.status.list = Object.keys(data[0].value);
    });

    this.rest.getByField(TABLE_TYPE.SETTINGS, "name", "SAVED_FILTER")
      .subscribe((data: SettingModel<TicketFitler>[]) => {
        this.filterList = data || [];
      });

    this.user.getAllUsers().subscribe((users: UserModel[]) => {
      this.defaultQuery.value.filter.user_own.list = users.map(us => {
        return {
          first_name: us.first_name,
          last_name: us.first_name,
          _id: us._id
        }
      });
    });
    this.currentFilter = Object.assign({}, this.defaultQuery.value);
  }

  ngOnDestroy(): void {
    this.state.tlr = {
      list: this.ticketList,
      filter: this.currentFilter
    }
  }

  onClickTicket(ticket: TicketModel) {
    this.nav.emit(ticket);
  }

  onClearFilterOption(name: string) {
    this.currentFilter.filter[name].low = "";
    this.currentFilter.filter[name].high = "";
    this.currentFilter.filter[name].active = '';
  }

  dateSelection(item: string) {
    this.currentFilter.filter.create_date.low = "";
    this.currentFilter.filter.create_date.high = "";
    this.currentFilter.filter.create_date.active = item;
    if (item.indexOf("day") > -1) {
      this.currentFilter.filter.create_date.low = item;
    }
  }

  ngOnInit() {
    if (this.state && this.state.tlr) {
      this.ticketList = this.state.tlr.list;
      this.currentFilter = this.state.tlr.filter;
    } else {
      this.executeQuery();
    }

  }
  getToday() {
    return new Date();
  }

  resetAll() {
  }

  onExportToExcel() {
    let pipe = new DatePipe('en-US');
    var excelJSON = this.ticketList.map((row: TicketModel, i) => {
      return {
        "Sl no": i + 1,
        "Application Date": pipe.transform(row.history.create.date, 'longDate'),
        "Application Number": row.requestor.app_no,
        "Ticekt Number": row.ticket_num,
        "Survey No": row.property_details.patta,
        "Village": row.property_details.village,
        "Hobli": row.property_details.hobli,
        "Taluk": row.property_details.taluk,
        "Applicant Name": row.requestor.full_name,
        "Mobile Number": row.requestor.contact,
        "Surveyor Name": row.stages.current.user ? row.stages.current.user.first_name + " " + row.stages.current.user.last_name : '',
        "Status": row.status.replace(/_/g, ' '),
        "Priority": row.priority,
        "Type": row.request_type,
        "Due Date": pipe.transform(row.eta_date, 'longDate')
      }
    });

    this.ticket.exportToExcel(excelJSON, "result_ticket");
  }

  onPrintRequest() {
    window.print();
  }

  switchFilter(filter: SettingModel<TicketFitler>, event) {
    event.preventDefault();
    this.currentFilter = filter.value;
    this.executeQuery();
  }

  executeQuery() {
    this.filterLoading = 1;
    this.ticket.getTicketReport(this.currentFilter)
      .subscribe((ticket: TicketModel[]) => {
        this.ticketList = ticket;
        this.filterLoading = ticket.length ? 2 : 3;
      });
  }

  deleteFilter() {
    if (this.currentFilter.name) {
      var arr = this.filterList.filter((item) => {
        return item.value.name == this.currentFilter.name;
      });
      if (arr.length && arr[0]._id) {
        this.rest.deleteById(TABLE_TYPE.SETTINGS, arr[0]._id).subscribe();
        this.filterList = this.filterList.filter((item) => {
          return item.value.name != this.currentFilter.name;
        });

        this.currentFilter = Object.assign({}, this.defaultQuery.value);
        this.executeQuery();
      }
    }
  }

  saveFilter() {
    if (this.currentFilter.name) {
      var arr = this.filterList.filter((item) => {
        return item.value.name == this.currentFilter.name;
      });
      if (arr.length) {
        arr[0].value = this.currentFilter;
        this.rest.updateById(TABLE_TYPE.SETTINGS, arr[0]._id, arr[0]).subscribe();
      } else {
        let sett = new SettingModel<TicketFitler>();
        sett.value = this.currentFilter;
        sett.name = "SAVED_FILTER";
        this.filterList.push(sett);
        this.rest.create(TABLE_TYPE.SETTINGS, sett).subscribe();
      }

    }
  }

}




export class TicketConfig {
  create_date: TicketConfigType;
  user_own: TicketConfigType;
  status: TicketConfigType;
  type: TicketConfigType;
  priority: TicketConfigType;
  village: TicketConfigType;
  constructor() {
    this.create_date = new TicketConfigType("history.create.date");
    this.user_own = new TicketConfigType("stages.current.user");
    this.status = new TicketConfigType("status");
    this.type = new TicketConfigType("request_type");
    this.priority = new TicketConfigType("priority");
    this.village = new TicketConfigType("property_details.village");

    this.create_date.list = [
      "Today",
      "Yesterday",
      // "Selected Date",
      "Date Range"
    ];
    this.type.list = ["SURVEY", "COMPLAIN"]
    this.priority.list = ["LOW", "MEDIUM", "HIGH", "VERY_HIGH", "ESCALATE"]
  }
}

export class TicketFitler {
  name: string;
  filter: TicketConfig;
  constructor() {
    this.name = "";
    this.filter = new TicketConfig();
  }
}

export class TicketConfigType {
  list: any[];
  active: any;
  type: string = "";
  low: string = "";
  high: string = "";
  constructor(t?: string) {
    this.type = t;
    this.list = [];
  }
}