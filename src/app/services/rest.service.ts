import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';

@Injectable()
export class RestService {

  constructor(private config: ConfigService, private http: HttpClient) {
  }

  getById<T>(table: TABLE_TYPE, id: string): Observable<T> {
    return this.http.get<T>(`${this.config.getApi()}/rest/${table}/${id}`);
  }
  getByField<T>(table: TABLE_TYPE, key: string, value: string): Observable<T> {
    return this.http.get<T>(`${this.config.getApi()}/rest/${table}/${key}/${value}`);
  }
  getAll<T>(table: TABLE_TYPE): Observable<T> {
    return this.http.get<T>(`${this.config.getApi()}/rest/${table}`);
  }
  updateById<T>(table: TABLE_TYPE, id: string, data: T): Observable<T> {
    return this.http.post<T>(`${this.config.getApi()}/rest/${table}/${id}`, data);
  }
  deleteById<T>(table: TABLE_TYPE, id: string): Observable<T> {
    return this.http.delete<T>(`${this.config.getApi()}/rest/${table}/${id}`);
  }
  create<T>(table: TABLE_TYPE, data: T): Observable<T> {
    return this.http.put<T>(`${this.config.getApi()}/rest/${table}`, data);
  }
  createDocument(data: any) {
    return this.http.post(`${this.config.getApi()}/file/document`, data);
  }
  getAllDocuments() {
    return this.http.get(`${this.config.getApi()}/file/documents`);
  }
  getDocumentsByRole(roleId: string) {
    return this.http.get(`${this.config.getApi()}/file/document/${roleId}`);
  }
  deleteDocument(docId: string) {
    return this.http.delete(`${this.config.getApi()}/file/document/${docId}`);
  }
}

export enum TABLE_TYPE {
  ROLE = "Role",
  SETTINGS = "Settings",
  DOC_HUB = "DocHub",
  COMM = "Communication"
}
