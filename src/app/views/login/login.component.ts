import { Router } from '@angular/router';
import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'login',
    templateUrl: 'login.template.html'
})
export class loginComponent implements OnInit {

    public userName: string;
    public password: string;
    public isLoading:boolean=false;
    public errorLogin:string;
    constructor(private userService: UserService, private router: Router) { }
    ngOnInit(): void {
        if (this.userService.hasSession()) {
            this.router.navigate(['/app/tickets']);
        }
    }

    login() {
        if (this.userName && this.password) {
            this.isLoading=true;
            this.userService.login(this.userName, this.password).subscribe((data) => {
                if (data.success) {
                    this.router.navigate(['/app/tickets']);
                }else{
                    this.errorLogin=data.message;
                    this.isLoading=false;
                }                
            });
        }
    }
}