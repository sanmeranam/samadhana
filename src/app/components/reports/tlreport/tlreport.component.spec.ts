import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TlreportComponent } from './tlreport.component';

describe('TlreportComponent', () => {
  let component: TlreportComponent;
  let fixture: ComponentFixture<TlreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TlreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TlreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
