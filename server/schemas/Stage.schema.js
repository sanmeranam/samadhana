var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Stage = new Schema({
    user: { type: Schema.Types.ObjectId, ref: "user" },
    action: { type: string, enum: ["CREATE", "MODIFY", "APPROVAL", "ADMIN"] },
    in_date: { type: Date },
    eta_date: { type: Date },
    out_date: { type: Date }
});



module.exports = mongoose.model("stage", Stage);