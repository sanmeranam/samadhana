import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanActivateChild } from "@angular/router";
import { Observable } from "rxjs";
import { UserService } from './services/user.service';

@Injectable()
export class AlwaysAuthGuard implements CanActivate {

    constructor(private router: Router, private userConfig: UserService) {
    }

    canActivate(activeRoute: ActivatedRouteSnapshot, routeSnap: RouterStateSnapshot) {
        if (localStorage.getItem('_user')) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    }
}



@Injectable()
export class AlwaysAuthChildrenGuard implements CanActivateChild {
    constructor(private userConfig: UserService, private router: Router, ) {

    }

    canActivateChild(activeRoute: ActivatedRouteSnapshot, routeSnap: RouterStateSnapshot) {
        return this.userConfig.me(false, routeSnap.url).map((user) => {
            if (user.data == "/app/users" && !user.role.access.users) {
                this.router.navigate(['/app/tickets']);
                return false;
            }
            if (user.data == "/app/settings/commun" && !user.role.access.settings) {
                this.router.navigate(['/app/tickets']);
                return false;
            }
            if (user.data == "/app/reports/all" && !user.role.access.report.all) {
                this.router.navigate(['/app/reports/user']);
                return false;
            }
            return true
        }).catch((err: any, catc: Observable<any>) => {
            if (err.status === 401) {
                this.router.navigate(['/login']);
            }
            return Observable.empty();
        });
    }
}