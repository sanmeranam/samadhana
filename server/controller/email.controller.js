var nodemailer = require("nodemailer");
var Settings = require("../schemas/Settings.schema");

module.exports = emailcom = {
    _transporter: null,
    _from: '',
    _init: function () {
        Settings.findOne({ name: "mail_config" })
            .then(function (result) {
                if (result.value.service) {
                    emailcom._transporter = nodemailer.createTransport({
                        service: result.value.service,
                        auth: {
                            user: result.value.username,
                            pass: result.value.password
                        }
                    });
                } else {
                    emailcom._transporter = nodemailer.createTransport({
                        host: result.value.host,
                        port: result.value.port,
                        secure: result.value.secure,
                        auth: {
                            user: result.value.username,
                            pass: result.value.password
                        }
                    });
                }
                emailcom._from = result.value.from;

            });
    },
    sendEmail: function (to, subject, content, callback) {
        var mailOptions = {
            from: emailcom._from,
            to: to,
            subject: subject,
            html: content
        };
        emailcom._transporter.sendMail(mailOptions, callback);
    }
}

emailcom._init();