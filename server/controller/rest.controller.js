
var fnGetClass = function (sName) {
    try {
        return require("../schemas/" + sName+".schema");
    } catch (e) {
        return null;
    }
};

var fnPromiseToResponse = function (oProms, res) {
    oProms.then(function (data) {
        res.json(data);
    }).catch(function (e) {
        res.json(e);
    });
};

module.exports = {
    getAll: function (req, res) {
        var sTable = req.params.table;
        var cClass = fnGetClass(sTable);
        if (cClass) {
            fnPromiseToResponse(cClass.find({}), res);
        } else {
            res.json({
                error: "Table not found."
            })
        }
    },
    getById: function (req, res) {
        var sTable = req.params.table;
        var cClass = fnGetClass(sTable);
        if (cClass) {
            fnPromiseToResponse(cClass.findById(req.params.id), res);
        } else {
            res.json({
                error: "Table not found."
            })
        }
    },
    getByField: function (req, res) {
        var sTable = req.params.table;
        var cClass = fnGetClass(sTable);
        if (cClass) {
            var query = {};
            query[req.params.key] = new RegExp(req.params.val, "i");
            fnPromiseToResponse(cClass.find(query), res);
        } else {
            res.json({
                error: "Table not found."
            })
        }
    },
    create: function (req, res) {
        var sTable = req.params.table;
        var cClass = fnGetClass(sTable);
        if (cClass) {
            fnPromiseToResponse(new cClass(req.body).save(), res);
        } else {
            res.json({
                error: "Table not found."
            })
        }
    },
    update: function (req, res) {
        var sTable = req.params.table;
        var cClass = fnGetClass(sTable);
        if (cClass) {
            fnPromiseToResponse(cClass.findByIdAndUpdate(req.params.id, req.body), res);
        } else {
            res.json({
                error: "Table not found."
            })
        }
    },
    delete: function (req, res) {
        var sTable = req.params.table;
        var cClass = fnGetClass(sTable);
        if (cClass) {
            fnPromiseToResponse(cClass.findByIdAndRemove(req.params.id), res);
        } else {
            res.json({
                error: "Table not found."
            })
        }
    }
};