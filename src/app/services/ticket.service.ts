import { TicketFitler } from './../components/reports/tlreport/tlreport.component';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class TicketService {

  constructor(private config: ConfigService, private http: HttpClient) {
  }

  getFlowConfig() {
    return this.http.get(`${this.config.getApi()}/flow`);
  }

  createTicket() {
    return this.http.get(`${this.config.getApi()}/ticket/create`);
  }

  saveTicket(data) {
    return this.http.post(`${this.config.getApi()}/ticket/create`, data);
  }

  getTicket(id) {
    return this.http.get(`${this.config.getApi()}/ticket/get/${id}`);
  }

  getTicketAll() {
    return this.http.get(`${this.config.getApi()}/ticket/get`);
  }

  getTicketCount(ticketType: string, district?: string, taluk?: string) {
    if (district && taluk) {
      return this.http.post(`${this.config.getApi()}/ticket/get/count/${ticketType}`, {
        district,
        taluk
      });
    } else {
      return this.http.get(`${this.config.getApi()}/ticket/get/count/${ticketType}`);
    }
  }
  getTicketAllByQuery(query: any) {
    return this.http.post(`${this.config.getApi()}/ticket/get`, query);
  }

  doActionTicket(action, id, data) {
    return this.http.post(`${this.config.getApi()}/ticket/action/${action}/${id}`, data);
  }

  updateTicket(id, data) {
    return this.http.post(`${this.config.getApi()}/ticket/update/${id}`, data);
  }

  updatePartialTicket(id, data) {
    return this.http.post(`${this.config.getApi()}/ticket/partupdate/${id}`, data);
  }

  ticketSearch(data) {
    return this.http.post(`${this.config.getApi()}/ticket/search`, data);
  }
  webviewData() {
    return this.http.get(`${this.config.getApi()}/ticket/webview`);
  }

  getTicketReport(tf: TicketFitler) {
    return this.http.post(`${this.config.getApi()}/report/tickets`, tf.filter);
  }

  getUserReport(tf: TicketFitler) {
    return this.http.post(`${this.config.getApi()}/report/users`, tf);
  }

  exportToExcel(json: any[], excelFileName: string) {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
}
