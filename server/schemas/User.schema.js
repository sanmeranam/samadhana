var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt');


var User = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    avtar: {
        type: String,
        default: ""
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        // required: true,
        match: [
            /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
        ]
    },
    contact: {
        type: String,
        default: ""
    },
    address: {
        house: String,
        area: String,
        village: String,
        city: String,
        state: String,
        country: String
    },
    description: {
        type: String,
        default: ""
    },
    language: {
        type: String,
        default: "en"
    },
    role: { type: Schema.Types.ObjectId, ref: 'roles' },
    active: {
        type: Boolean,
        default: true
    },
    activity: Schema.Types.Mixed,
    created: {
        type: Date,
        default: Date.now
    },
    modified: {
        type: Date
    }
});

// Hash the user's password before inserting a new user
User.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        user.modified = new Date;
        return next();
    }
});


// Compare password input to password saved in database
User.methods.comparePassword = function (pw, cb) {
    bcrypt.compare(pw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};


module.exports = mongoose.model("user", User);