var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Memo = new Schema({
    user: { type: Schema.Types.ObjectId, ref: "user" },
    date: { type: Date, default: Date.now },
    content: { type: string, default: '' },
    memo_type: { type: string, enum: ["REPLY", "COMMENT", "ISSUE"] },
    files: [
        {
            name: { type: string },
            path: { type: string }
        }
    ]
});



module.exports = mongoose.model("memo", Memo);