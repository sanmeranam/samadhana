var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Setting = new Schema({
    name: { type: String },
    value: { type: Schema.Types.Mixed }
});



module.exports = mongoose.model("setting", Setting);