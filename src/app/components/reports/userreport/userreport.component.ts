import { RoleModel } from './../../../models/user.model';
import { UserService } from './../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../../models/user.model';

@Component({
    selector: 'app-userreport',
    templateUrl: './userreport.component.html',
    styleUrls: ['./userreport.component.scss']
})
export class UserreportComponent implements OnInit {
    currentFilter: any;
    userList: any[];
    roleList: RoleModel[];

    filterLoading: number = 0;
    constructor(private user: UserService) {
        this.clearFilter();

        this.user.getAllRoles().subscribe((roles: RoleModel[]) => {
            this.roleList = roles;
        });
    }

    ngOnInit() {
    }

    executeQuery() {
        this.user.getUserReports(this.currentFilter).subscribe((userData: any[]) => {
            this.userList = userData;
        });
    }

    getToday() {
        return new Date()
    }

    onClickUser() {

    }

    onPrintRequest() {

    }

    onExportToExcel() {

    }

    clearFilter() {
        this.currentFilter = {
            start_date: '',
            end_date: '',
            role: ''
        };
        this.executeQuery()
    }
}
