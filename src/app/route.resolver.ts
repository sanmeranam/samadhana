import { flatMap } from 'rxjs/operators';
import { ActivatedRouteSnapshot } from '@angular/router';
import { TicketService } from './services/ticket.service';
import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from './services/user.service';
import 'rxjs/Rx';
import { TicketModel } from './models/ticket.model';

@Injectable()
export class AppResolver implements Resolve<Observable<string>> {
    constructor(private userService: UserService, private router: Router) { }

    resolve() {
        return this.userService.sessionForRoute()
            .catch((err: any, catc: Observable<any>) => {
                if (err.status === 401) {
                    this.router.navigate(['/login']);
                }
                return Observable.empty();
            });
    }
}

@Injectable()
export class NewTicketResolver implements Resolve<Observable<string>> {
    constructor(private userService: UserService, private router: Router) { }

    resolve() {
        return new Observable<any>((observer) => {
            observer.next(new TicketModel());
        });
    }
}

@Injectable()
export class OpenResolver implements Resolve<Observable<any>> {
    constructor(private ticket: TicketService) { }

    resolve(route: ActivatedRouteSnapshot) {
        return this.ticket.getTicket(route.paramMap.get('id'));
    }
}