export class EmailTemaplateModel {
    type: string;
    name: string;
    subject: string;
    content: string;
}

export class EmailConfig {
    host: string;
    port: number;
    service: string;
    secure: boolean;
    username: string;
    password: string;
}