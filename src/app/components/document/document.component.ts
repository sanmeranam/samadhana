import { AlertsService } from './../../services/alerts.service';
import { ConfigService } from './../../services/config.service';
import { FileModel } from './../../models/ticket.model';
import { RoleTypeEnum, RoleModel } from './../../models/user.model';
import { DocHub } from './../../models/dochub.mode';
import { Component, OnInit } from '@angular/core';
import { RestService, TABLE_TYPE } from '../../services/rest.service';
import { UserService } from '../../services/user.service';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {

  public currentUser: UserModel;
  public documents: DocHub[];
  public newFile: DocHub;
  public roles: RoleModel[];
  public selectedRoles: any = {};

  constructor(private rest: RestService, private user: UserService, public config: ConfigService, private alert: AlertsService) {
    this.resetFile();
    this.user.me().subscribe((user: UserModel) => {
      this.currentUser = user;
      this.loadDocumentList();
    });

    this.user.getAllRoles().subscribe((roles: RoleModel[]) => {
      this.roles = roles;
      roles.forEach(item => {
        this.selectedRoles[item.name] = {
          value: false,
          key: item._id
        };
      });
    });

    this.documents = [];
  }

  ngOnInit() {
  }

  private resetFile() {
    this.newFile = null;
    Object.keys(this.selectedRoles).forEach((item) => {
      this.selectedRoles[item].value = false;
    });
  }

  loadDocumentList() {
    if (this.currentUser.role.type === RoleTypeEnum.ADMIN) {
      this.loadAllDocuments();
    } else {
      this.loadRoleBasedDocuments()
    }
  }


  onNewFileCreate() {
    this.newFile = new DocHub();
  }
  onCancelFileCreate() {
    this.resetFile();
    this.alert.warnging("<strong>Canceled!</strong> Create Document.");
  }

  onChangeValue(name) {
    if (this.selectedRoles[name].value) {
      this.newFile.access.push(this.selectedRoles[name].key)
    } else {
      let index = this.newFile.access.indexOf(this.selectedRoles[name].key);
      if (index > -1) {
        this.newFile.access.splice(index, 1);
      }
    }
  }

  onSaveNewFile() {
    if (this.newFile.file && this.newFile.access.length > 0) {
      this.newFile.file = this.newFile.file._id;
      this.rest.createDocument(this.newFile).subscribe(result => {
        this.resetFile();
        this.loadDocumentList();
        this.alert.success("<strong>Success!</strong> Document Added.");
      });
    } else {
      this.alert.error("<strong>Failed!</strong> Either file not added or target user group not selected.");
    }

  }

  onDeleteFile(Doc, event) {

    this.alert.confirm("Are sure want delete this document?", "Delete Document")
      .subscribe((result) => {
        if (result) {
          this.rest.deleteDocument(Doc._id).subscribe(() => {
            this.loadDocumentList();
            this.alert.success("<strong>Success!</strong> Document Deleted.");
          });
        }
      });
  }

  fileUpload(files: FileList) {
    this.user.uploadFile(files.item(0)).subscribe((files: FileModel[]) => {
      this.newFile.file = files[0];
    });
  }

  getDownLoadLink(Doc) {
    return this.config.getApi() + '/file/' + Doc.file._id
  }

  private loadRoleBasedDocuments() {
    this.rest.getDocumentsByRole(this.currentUser.role._id)
      .subscribe((list: DocHub[]) => {
        this.documents = list;
      });
  }

  private loadAllDocuments() {
    this.rest.getAllDocuments()
      .subscribe((list: DocHub[]) => {
        this.documents = list;
      });
  }

  getIconByMimeType(mime: string) {
    switch (mime) {
      case "application/vnd.ms-excel":
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
        return "fa-file-excel-o text-green";
      case "application/pdf":
        return "fa-file-pdf-o text-red";
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
        return "fa-file-word-o text-primary";
      case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
        return "fa-file-powerpoint-o text-orange"
      default:
        return "fa-file-text text-info";
    }
  }
}
