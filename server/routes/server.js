var express = require('express');
var router = express.Router();

var UserController = require("../controller/user.controller");
var RestController = require("../controller/rest.controller");
var SessionController = require("../controller/session.controller");
var FileService = require("../controller/file.controller");
var TicketController = require("../controller/ticket.controller");
var AccessLog = require("../controller/access_log.controller");
var SMSController = require("../controller/sms.controller");
var ReportController = require("../controller/report.controller");

//Session

router.post('/login', SessionController.login);
router.get('/logout', SessionController.checkSession, SessionController.logout);
router.get('/me', AccessLog.internal, SessionController.checkSession, SessionController.me);



//User 
router.get('/user', SessionController.checkSession, UserController.userGetAll);
router.post('/user/byStage', SessionController.checkSession, UserController.userByStage);
router.get('/roles', SessionController.checkSession, UserController.userRoleGetAll);

router.get('/user/:id', SessionController.checkSession, UserController.userGetById);
router.get('/roles/:id', SessionController.checkSession, UserController.userRoleGetById);


router.post('/user', SessionController.checkSession, UserController.userCreate);
router.post('/user_report', SessionController.checkSession, UserController.getUserReport);
router.post('/roles', SessionController.checkSession, UserController.userRoleCreate);

router.post('/user/:id', SessionController.checkSession, UserController.userUpdate);
router.post('/roles/:id', SessionController.checkSession, UserController.userRoleUpdate);

router.delete('/user/:id', SessionController.checkSession, UserController.userDelete);
router.delete('/roles/:id', SessionController.checkSession, UserController.userRoleDelete);


router.get('/ticket/create', SessionController.checkSession, TicketController.createTicket);
router.post('/ticket/create', SessionController.checkSession, TicketController.saveNew);
router.get('/ticket/get', SessionController.checkSession, TicketController.getAllTicket);
router.post('/ticket/get', SessionController.checkSession, TicketController.getAllTicketByQuery);
router.get('/ticket/get/count/:type', SessionController.checkSession, TicketController.getBasicCouts);
router.post('/ticket/get/count/:type', SessionController.checkSession, TicketController.getBasicCouts);
router.get('/ticket/get/:id', SessionController.checkSession, TicketController.getTicket);
router.post('/ticket/update/:id', SessionController.checkSession, TicketController.update);
router.post('/ticket/partupdate/:id', SessionController.checkSession, TicketController.ticketPartialUpdate);
router.post('/ticket/action/:action/:id', SessionController.checkSession, TicketController.actionPerformed);
router.post('/ticket/search', AccessLog.track, TicketController.userSearchText);
router.get('/ticket/webview', AccessLog.log, TicketController.getLandingPageData);


//Rest
router.get('/rest/:table', SessionController.checkSession, RestController.getAll);
router.get('/rest/:table/:id', SessionController.checkSession, RestController.getById);
router.get('/rest/:table/:key/:val', SessionController.checkSession, RestController.getByField);
router.put('/rest/:table', SessionController.checkSession, RestController.create);
router.post('/rest/:table/:id', SessionController.checkSession, RestController.update);
router.delete('/rest/:table/:id', SessionController.checkSession, RestController.delete);


//Report

router.post('/report/tickets', ReportController.getTicketList);
router.post('/report/users', ReportController.getTicketList);



//File Services

router.post('/file', FileService.fileUpload);
router.post('/file/document', FileService.createDocument);
router.get('/file/document/:roleId', FileService.getDocumentsByRole);
router.delete('/file/document/:id', FileService.deleteDocument);
router.get('/file/documents', FileService.getAllDocuments);
router.get('/file/:id', FileService.fileDownload);

router.get('/sms/:to/:type', SMSController.trialSend);

router.get('/flow', function (req, res) {
    res.json(req.flowConfig);
});

module.exports = router;
