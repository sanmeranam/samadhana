var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Communi = new Schema({
    type: { type: String, enum: ["SMS", "EMAIL"] },
    to: { type: String },
    ticket: { type: Schema.Types.ObjectId, ref: "ticket" },
    status: { type: String },
    date: { type: Date, default: Date.now },
    content: { type: String },
    response: { type: Schema.Types.Mixed }
});



module.exports = mongoose.model("communication", Communi);