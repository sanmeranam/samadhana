import { Directive, ElementRef, Input, Output, EventEmitter } from '@angular/core';

declare var jQuery: any;
@Directive({
  selector: '.appDatepicker'
})
export class DatepickerDirective {
  @Input() startDate: string;
  @Input() endDate: string;
  @Input() format: string;
  @Input() key: string;
  @Input() ref: any;
  @Output() select = new EventEmitter();

  private element: any;
  constructor(private el: ElementRef) {
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    this.element = jQuery(el.nativeElement).datepicker({
      startDate: this.startDate,
      endDate: this.endDate,
      orientation: "bottom auto",
      format: this.format || {
        toDisplay: (date, format, language) => {
          var d = new Date(date);
          this.ref[this.key] = date;
          return `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`;
        },
        toValue: (date, format, language) => {
          this.ref[this.key] = date;
          return date;
        }
      }
    }).on("changeDate", (e) => {
      this.element.datepicker('hide');
      this.select.emit(e);
    });
    if (this.ref && this.key && this.ref[this.key])
      jQuery(el.nativeElement).datepicker('update', new Date(this.ref[this.key]));
  }

}
