var AccessLog = require("../schemas/AccessLog.schema");

module.exports = Ac = {
    log: function (req, res, next) {
        Ac._record(req, "SITE");
        next();
    },
    internal: function (req, res, next) {
        Ac._record(req, "INTERNAL");
        next();
    },
    track: function (req, res, next) {
        Ac._record(req, "TRACK");
        next();
    },
    _record: function (req, type) {
        var sock = req.socket;
        var remoteAddress = sock.socket ? sock.socket.remoteAddress : sock.remoteAddress;
        new AccessLog({
            remote: remoteAddress,
            count: 1,
            date: new Date(),
            type: type,
            agent: req.get('User-Agent')
        }).save();
    }


}