import { AlertsService } from './../../../services/alerts.service';
import { Component, OnInit } from '@angular/core';
import { SMSSettingsModel, TransactionsModel, SMSTemplateModel, SMSSettingsParam, SettingModel } from './sms.model';
import { RestService, TABLE_TYPE } from '../../../services/rest.service';

@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.scss']
})
export class SmsComponent implements OnInit {

  smsSetting: SettingModel<SMSSettingsModel>;
  smsTransactions: TransactionsModel[];
  smsTemplates: SettingModel<SMSTemplateModel[]>;

  activeTemplate: SMSTemplateModel;

  constructor(private rest: RestService, private alert: AlertsService) {
    this.rest.getByField(TABLE_TYPE.SETTINGS, "name", "sms_template")
      .subscribe((result: SettingModel<SMSTemplateModel[]>[]) => {
        this.smsTemplates = result[0];
        this.activeTemplate = this.smsTemplates.value.length ? this.smsTemplates.value[0] : new SMSTemplateModel();
      });

    this.rest.getByField(TABLE_TYPE.SETTINGS, "name", "sms_config")
      .subscribe((result: SettingModel<SMSSettingsModel>[]) => {
        this.smsSetting = result[0];
      });

    this.rest.getByField(TABLE_TYPE.COMM, "type", "SMS")
      .subscribe((result: TransactionsModel[]) => {
        this.smsTransactions = result;
      });
  }


  onSelectTemplate(temp: SMSTemplateModel) {
    this.activeTemplate = temp;
  }

  ngOnInit() {

  }

  onAddParam() {
    this.smsSetting.value.params.push(new SMSSettingsParam("", ""));
  }
  onRemoveParam(index) {
    this.smsSetting.value.params.splice(index, 1);
  }

  // onNewTemplate() {
  //   let temp = new SMSTemplateModel();
  //   temp.name = "template_" + (Math.round(Math.random() * 55));
  //   temp.content = "sample content";
  //   this.activeTemplate = temp;
  //   this.smsTemplates.value.push(temp);
  // }
  onSaveSMSSettings() {
    this.rest.updateById(TABLE_TYPE.SETTINGS, this.smsSetting._id, this.smsSetting).subscribe(() => {
      this.alert.success("<strong>Success!</strong> Setting updated.")
    });
  }

  onSaveSMSTemplates() {
    this.rest.updateById(TABLE_TYPE.SETTINGS, this.smsTemplates._id, this.smsTemplates).subscribe(() => {
      this.alert.success("<strong>Success!</strong> Templates updated.")
    });
  }
}