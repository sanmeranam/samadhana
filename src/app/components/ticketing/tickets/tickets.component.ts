import { UserModel, RoleTypeEnum } from './../../../models/user.model';
import { ConfigService } from './../../../services/config.service';
import { UserService } from './../../../services/user.service';
import { TicketService } from './../../../services/ticket.service';
import { Router } from '@angular/router';
import { TicketModel, PriorityEnum } from './../../../models/ticket.model';
import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, flatMap, switchMap } from 'rxjs/operators';
declare var jQuery: any;

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit, AfterViewChecked {


  items = [];
  ticketList: TicketModel[] = [];
  priorityEnum = Object.keys(PriorityEnum);
  view: string = "CARD";
  wideScreen: boolean = false;
  userByStageCache = {};
  filterList: any[];
  statusList: string[];
  currentFilter: any;
  isLoadingList: boolean = false;
  flowConfig: any;
  filterQuery: any;
  filterDate: any;
  userList: UserModel[];
  allCounts: any;
  currentUser: UserModel;
  currentType: string = "SURVEY";

  constructor(private ticketService: TicketService, private userServ: UserService, private config: ConfigService) {

    this.filterList = [
      {
        name: "Ticket ID",
        key: "ticket_num"
      },
      {
        name: "Name",
        key: "requestor.full_name"
      },
      {
        name: "Survey No",
        key: "property_details.patta"
      },
      {
        name: "Contact No",
        key: "requestor.contact"
      }
    ]

    this.currentFilter = {
      text: "",
      filter: this.filterList[0],
      list: null
    }


    this.filterQuery = {};
    this.resetFilterDate();
    this.userSearch();
    this.loadTicketCounts();
    this.loadTicketsByType(this.currentType, true);


    this.userServ.me().subscribe((user: UserModel) => {
      this.currentUser = user;
      this.loadTicketsByType(this.currentType);
    });
  }

  public loadTicketsByType(type, noData?) {
    this.currentType = type;
    this.flowConfig = this.config.getFlow(type);
    this.statusList = Object.keys(this.config.getFlow(type).STATUS_MAP);
    if (noData) return;
    this.loadTicketList(!this.currentUser.role.access.assign);
    this.loadTicketCounts();
  }

  public loadTicketCounts() {
    if (this.currentUser && this.currentUser.role && this.currentUser.role.type == RoleTypeEnum.USER) {
      this.ticketService.getTicketCount(this.currentType, this.currentUser.role.district, this.currentUser.role.taluk).subscribe(count => {
        this.allCounts = count;
      });
    } else {
      this.ticketService.getTicketCount(this.currentType).subscribe(count => {
        this.allCounts = count;
      });
    }

  }

  getDepthColor(index) {
    let colors = ["label-light", "label-danger", "label-danger", "label-warning", "label-success", "label-info", "label-primary"]
    return colors[index + 2];
  }
  ngAfterViewChecked(): void {
    jQuery("span.pie").peity("pie", {
      fill: ['#1ab394', '#d7d7d7', '#ffffff']
    })
  }

  ngOnInit() {
    this.loadTicketList(!this.currentUser.role.access.assign);
  }

  loadTicketList(loadMyTicketsOnly: boolean) {
    this.isLoadingList = true;

    if (this.currentUser && (this.currentUser.role.actions.SURVEY || this.currentUser.role.actions.COMPLAIN)) {
      if (loadMyTicketsOnly) {
        this.filterQuery['stages.current.user'] = this.currentUser._id;
      }
      if (this.currentUser.role.type == RoleTypeEnum.USER) {
        this.filterQuery['property_details.district'] = this.currentUser.role.district;
        this.filterQuery['property_details.taluk'] = this.currentUser.role.taluk;
      }

      this.filterQuery.request_type = this.currentType;
      this.ticketService.getTicketAllByQuery(this.filterQuery).subscribe((data: TicketModel[]) => {
        this.ticketList = data;
        this.isLoadingList = false;
      });
    }
  }

  checkUserAccess(ticket) {
    return this.currentUser.role.access.assign == false ||
      ticket.status == "COMPLETED";
  }

  showUnassigned() {
    this.filterQuery = {
      'stages.current.user': null
    }
    this.loadTicketList(false);
  }
  showMyTickets() {
    this.loadTicketList(true);
  }


  resetFilterDate() {
    this.filterDate = {
      "history.create.date": {
        $gte: null,
        $lt: null
      }
    }
  }

  clearQuery(base, name) {
    delete (base[name]);
    this.loadTicketList(!this.currentUser.role.access.assign);
  }

  filterFromDate() {
    if (this.filterDate["history.create.date"].$gte
      && this.filterDate["history.create.date"].$lt) {
      this.filterQuery["history.create.date"] = this.filterDate["history.create.date"];
      this.loadTicketList(!this.currentUser.role.access.assign);
    }
  }

  userSearch() {
    this.userServ.getAllUsers().subscribe(data => {
      this.userList = data;
    });
  }


  onFilterGo() {
    if (this.currentFilter.text) {
      let low = this.currentFilter.filter.key;
      let high = '';
      if (this.currentFilter.filter.key.indexOf(".") > -1) {
        let filters = this.currentFilter.filter.key.split(".");
        low = filters[0];
        high = filters[1];
      }
      this.currentFilter.list = this.ticketList.filter((item) => {
        if (high) {
          return item[low][high].indexOf(this.currentFilter.text) > -1
        }
        return item[low].indexOf(this.currentFilter.text) > -1
      });
    }
  }

  onClearSearch() {
    this.currentFilter.list = null;
    this.currentFilter.text = '';
  }

  getList(): TicketModel[] {
    return this.currentFilter.list || this.ticketList;
  }

  loadUserByStage(ticket: TicketModel) {
    // if (this.userByStageCache[ticket.status]) {
    //   return
    // }
    this.userServ.getUsersByStageAllowed(ticket.status, ticket.request_type, ticket.property_details.district, ticket.property_details.taluk).subscribe(data => {
      this.userByStageCache[ticket.status] = data;
    })
  }

  updateUserAsignChanged(ticket) {
    if (ticket.stages.current.user) {
      var stages = JSON.parse(JSON.stringify(ticket.stages));//Object.assign({}, ticket.stages);
      stages.current.user = ticket.stages.current.user._id;
      this.ticketService.updatePartialTicket(ticket._id, { stages }).subscribe();
    }
  }

  updateProrityAsignChanged(ticket: TicketModel) {
    this.ticketService.updatePartialTicket(ticket._id, { priority: ticket.priority }).subscribe();
  }

}