var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Ticket = new Schema({
    ticket_num: {
        type: String,
        unique: true
    },
    subject_line: {
        type: String,
        default: ''
    },
    requestor: {
        full_name: { type: String, require: true, default: '' },
        father_name: { type: String, default: '' },
        app_no: { type: String, default: '' },
        contact: { type: String, default: '' },
        email: { type: String, default: '' }
    },
    property_details: {
        district: { type: String, require: true, default: '' },
        taluk: { type: String, require: true, default: '' },
        hobli: { type: String, require: true, default: '' },
        village: { type: String, require: true, default: '' },
        patta: { type: String, require: true, default: '' }
    },
    priority: {
        type: String,
        enum: ["LOW", "MEDIUM", "HIGH", "VERY_HIGH", "ESCALATE"],
        default: "MEDIUM"
    },
    status: {
        type: String,
        default: "NEW"
    },
    request_type: {
        type: String,
        default: "SURVEY"
    },
    eta_date: { type: Date, require: true },
    history: {
        create: {
            date: { type: Date, default: Date.now },
            user: { type: Schema.Types.ObjectId, ref: "user" },
            comment: { type: String, default: "" }
        },
        modified: [
            {
                date: { type: Date, default: Date.now },
                user: { type: Schema.Types.ObjectId, ref: "user" },
                comment: { type: String, default: "" }
            }
        ]
    },
    stages: {
        current: {
            user: { type: Schema.Types.ObjectId, ref: "user" },
            // user_group: { type: String, default: "" },
            action: { type: String, default: "" },
            status: { type: String, default: "" },
            in_date: { type: Date },
            eta_date: { type: Date },
            out_date: { type: Date }
        },
        history: [
            {
                user: { type: Schema.Types.ObjectId, ref: "user" },
                // user_group: { type: String, default: "" },
                action: { type: String, default: "" },
                status: { type: String, default: "" },
                in_date: { type: Date },
                eta_date: { type: Date },
                out_date: { type: Date }
            }
        ]
    },
    memo: [
        {
            user: { type: Schema.Types.ObjectId, ref: "user" },
            date: { type: Date, default: Date.now },
            content: { type: String, default: '' },
            memo_type: { type: String, enum: ["REPLY", "COMMENT", "ISSUE"] },
            files: [
                {
                    name: { type: String },
                    id: { type: String }
                }
            ]
        }
    ]
});

Ticket.pre('save', function (next) {
    var ticket = this;
    if (this.isNew) {

        ticket.ticket_num = Math.floor(Date.now() / 1000000).toString(36).toUpperCase() + (Date.now() - (Math.floor(Date.now() / 1000000) * 1000000));
        ticket.status = "NEW";
        next();
    } else {

        return next();
    }
});


module.exports = mongoose.model("ticket", Ticket);