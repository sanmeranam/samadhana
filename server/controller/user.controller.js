var User = require('../schemas/User.schema');
var Roles = require('../schemas/Role.schema');
var bcrypt = require('bcrypt');
var NotifManage = require("./notification.controller");

var fnPromiseToResponse = function (oProms, res) {
    oProms.then(function (data) {
        res.json(data);
    }).catch(function (e) {
        res.json(e);
    });
};

module.exports = {
    userGetAll: function (req, res) {
        fnPromiseToResponse(User.find({}, '-password'), res);
    },

    userRoleGetAll: function (req, res) {
        fnPromiseToResponse(Roles.find({}), res);
    },

    userGetById: function (req, res) {
        fnPromiseToResponse(User.findById(req.params.id, '-password')
            .populate('role'), res);
    },

    userRoleGetById: function (req, res) {
        fnPromiseToResponse(Roles.findById(req.params.id), res);
    },

    userCreate: function (req, res) {
        fnPromiseToResponse(new User(req.body).save(), res);
        NotifManage.dispatchUserDetailsUpdate(req.body);
    },

    userRoleCreate: function (req, res) {
        fnPromiseToResponse(new Roles(req.body).save(), res);
    },

    userUpdate: function (req, res) {
        req.body.modified = new Date();
        if (req.body.password) {
            NotifManage.dispatchUserDetailsUpdate(req.body);
            bcrypt.genSalt(10, function (err, salt) {
                if (err) {
                    return next(err);
                }
                bcrypt.hash(req.body.password, salt, function (err, hash) {
                    if (err) {
                        return next(err);
                    }
                    req.body.password = hash;
                    fnPromiseToResponse(User.findByIdAndUpdate(req.params.id, req.body, { runValidators: true }), res);
                });
            });
        } else {
            fnPromiseToResponse(User.findByIdAndUpdate(req.params.id, req.body, { runValidators: true }), res);
        }

    },
    userRoleUpdate: function (req, res) {
        fnPromiseToResponse(Roles.findByIdAndUpdate(req.params.id, req.body), res);
    },

    userDelete: function (req, res) {
        fnPromiseToResponse(User.findByIdAndRemove(req.params.id), res);
    },
    userRoleDelete: function (req, res) {
        fnPromiseToResponse(Roles.findByIdAndRemove(req.params.id), res);
    },
    userByStage: function (req, res) {
        var stage = req.body.stage;
        var type = req.body.type;
        var findOpt = {};
        if (req.body.district && req.body.taluk) {
            findOpt["district"] = req.body.district;
            findOpt["taluk"] = req.body.taluk;
        }
        findOpt['actions.' + type + '.' + stage] = true;
        var oProm = Roles.find(findOpt).then(function (aData) {
            aData = aData.map(function (v) { return v._id });
            return User.find({ role: { $in: aData } });
        });

        fnPromiseToResponse(oProm, res);
    },
    getUserReport: function (req, res) {
        /**
         * First Name
         * Last Name
         * avtar
         * role
         * mobile/email
         * 
         * Total Processed
         * Total in Progress
         * Avg holding time
         * Processed within due date
         * Processed beyond due date
         * 
         * 
         */
        res.json([{}, {}, {}]);
    }
};