# Local Setup

## Pre Requirements
* angular-cli (ng)
* mongodb (Database)

## Setup

1. Clone repository to your local folder.
2. use command `` npm install `` to resolve dependency node packages.
3. Then make sure that, mongodb is running with default port.
4. Now, we need to restore latest DB setup to your local database. Use command ``npm run restore``.
5. Both server and UI code setup is under one project setup. ``server`` folder is for backend services, ``src`` folder is for angular setup. To run locally as a development setup, we need to run both server and angular separatly.
6. Open 2 different ``cmd`` prompt under samadhana folder.
7. To run server user command ``npm run server``.
8. To run angular in ``dev`` mode, use `` ng serve --open``.
9. If you want run ui in production mode, run ``ng serve --prod --proxy-config proxy.conf.json``.
10. To do final build and release,    use `` ng build --prod ``. This command will clean folder ``server/public`` and create new minified files and will keep inside same folder.