import { ConfirmState } from './state.model';
import { AlertsService } from './../../../services/alerts.service';
import { Component, OnInit, ViewChild, TemplateRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html'
})
export class ConfirmComponent implements OnInit, AfterViewInit {


  @ViewChild('confirm') confirm: TemplateRef<any>;



  constructor(public state: ConfirmState) {

  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.state.tempalte = this.confirm;
  }

  closeMe(index) {
    this.state.alerts.splice(index, 1);
  }

  yes() {
    this.state.modal.close("yes");
  }

  no() {
    this.state.modal.close("no");
  }
}
