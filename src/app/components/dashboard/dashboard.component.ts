import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit {

  monthlyChart: any;
  countPie: any;
  constructor() { }

  ngOnInit() {
  }

  randomScalingFactor = () => {
    return Math.round(Math.random() * 100);
  };

  ngAfterViewInit(): void {
    this.drawPieChart();
    this.dragMonthlyChart();
  }

  private drawPieChart() {
    this.monthlyChart = new Chart('report_pie', {
      type: 'doughnut',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [
          {
            label: "Counts",
            data: [
              this.randomScalingFactor(),
              this.randomScalingFactor(),
              this.randomScalingFactor(),
              this.randomScalingFactor(),
              this.randomScalingFactor(),
            ],
            backgroundColor: [
              "#4D4D4D",
              "#5DA5DA",
              "#FAA43A",
              "#60BD68",
              "#F17CB0",
              "#B2912F",
              "#B276B2",
              "#DECF3F",
              "#F15854"
            ],
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }

  private dragMonthlyChart() {
    this.monthlyChart = new Chart('report_month', {
      type: 'bar',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [
          {
            label: "Tickets",
            data: [12, 34, 13, 42, 42, 41, 55, 12, 55, 56, 35, 75],
            backgroundColor: [
              "#00876c",
              "#4c9772",
              "#74a67b",
              "#97b589",
              "#b7c49b",
              "#d4d4b0",
              "#eee5c9",
              "#e7cfa7",
              "#e3b688",
              "#e19d6f",
              "#de805d",
              "#da6253",
              "#d43d51"
            ],
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }

}
