import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { UserService } from './../../../services/user.service';
import { TicketModel, MemoModel, MemoTypeEnum } from './../../../models/ticket.model';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfigService } from '../../../services/config.service';
import { TicketService } from '../../../services/ticket.service';
import { UserModel } from '../../../models/user.model';
import { RestService, TABLE_TYPE } from '../../../services/rest.service';
import { AlertsService } from '../../../services/alerts.service';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-newticket',
  templateUrl: './newticket.component.html',
  styleUrls: ['./newticket.component.css']
})
export class NewticketComponent implements OnInit, OnDestroy {

  public newTicket: TicketModel;
  public currentUser: UserModel;
  public replyMemo: MemoModel;
  private subUserService: any;
  private hasChangeSaved = false;
  public distctMap: any[];
  public talukMap: any[];
  public hobliMap: any[];
  public selectedDist: any;
  public selectedTaluk: any;
  public selectedHobli: any;
  public cV: any;

  constructor(private msg: AlertsService, private route: Router, rest: RestService, private userService: UserService, private modalService: NgbModal, public config: ConfigService, private ticketService: TicketService) {
    this.ticketService.createTicket().subscribe((arg: TicketModel) => {
      this.newTicket = arg;
    });

    this.subUserService = this.userService.me().subscribe((user: UserModel) => {
      this.currentUser = user
    });

    rest.getByField(TABLE_TYPE.SETTINGS, 'name', 'city').subscribe((district: any[]) => {
      if (district.length) {
        this.distctMap = district[0].value;
      }
    });
    this.resetValidation();
  }


  ngOnInit() {
    this.replyMemo = new MemoModel();
    this.replyMemo.user = this.currentUser;
    this.replyMemo.memo_type = MemoTypeEnum.ISSUE;
    this.replyMemo.files = [];
    this.replyMemo.date = new Date();

    this.distctMap = []

  }

  search(text: Observable<string>) {
    return text.pipe(debounceTime(200), distinctUntilChanged(), map(term => {
      term.length < 2 ? [] : ["asd", "asda", "asdas"];
    }))
  }

  // search = (text$: Observable<string>) =>
  //   text$.pipe(
  //     debounceTime(200),
  //     distinctUntilChanged(),
  //     map(term => term.length < 2 ? []:["asd","asda","asdas"])
  //       // : this.selectedHobli.village.filter(v => v.name.toLowerCase().indexOf(term.toLowerCase()) > -1).map(v=>v.name).slice(0, 10))
  //   )

  ngOnDestroy(): void {
    this.subUserService.unsubscribe();
    this.onDescardTicket();
  }

  onDescardTicket() {

  }


  onDistrictSelect(oDist) {
    if (this.newTicket.property_details.district) {
      this.selectedDist = this.distctMap.filter(item => item.name === this.newTicket.property_details.district)[0];
    }
  }

  onTalukSelect() {
    if (this.newTicket.property_details.taluk) {
      this.selectedTaluk = this.selectedDist.city.filter(item => item.name === this.newTicket.property_details.taluk)[0];
    }
  }

  onHobliSelect() {
    if (this.newTicket.property_details.hobli) {
      this.selectedHobli = this.selectedTaluk.hobli.filter(item => item.name === this.newTicket.property_details.hobli)[0];
    }
  }

  onCreateComplete(id: string) {
    this.route.navigate(['/app/tickets/details/' + id]);
  }

  private resetValidation() {
    this.cV = {
      dist: true,
      taluk: true,
      village: true,
      patta: true,
      hobli: true,
      content: true,
      subject: true,
      fullname: true,
      father: true,
      mobile: true,
      app: true
    };
  }


  private doCheckValidation() {
    this.cV.fullname = this.newTicket.requestor.full_name ? true : false;
    this.cV.father = this.newTicket.requestor.father_name ? true : false;
    this.cV.mobile = this.newTicket.requestor.contact ? true : false;
    this.cV.dist = this.newTicket.property_details.district ? true : false;
    this.cV.taluk = this.newTicket.property_details.taluk ? true : false;
    this.cV.village = this.newTicket.property_details.village ? true : false;
    this.cV.patta = this.newTicket.property_details.patta ? true : false;
    this.cV.content = this.replyMemo.content ? true : false;
    this.cV.subject = this.newTicket.subject_line ? true : false;
    this.cV.app = this.newTicket.requestor.app_no ? true : false;
    this.cV.hobli = this.newTicket.property_details.hobli ? true : false;

    return this.cV.fullname &&
      this.cV.father &&
      this.cV.mobile &&
      this.cV.dist &&
      this.cV.taluk &&
      this.cV.village &&
      this.cV.patta &&
      this.cV.content &&
      this.cV.subject &&
      this.cV.app &&
      this.cV.hobli;
  }

  onStageSave() {
    if (this.doCheckValidation()) {
      this.newTicket.memo.push(this.replyMemo);
      this.ticketService.saveTicket(this.newTicket).flatMap((data: any) => {
        return this.ticketService.doActionTicket("DO_CREATE", data._id, data);
      }).subscribe((ticket: any) => {
        this.hasChangeSaved = true;
        this.msg.success("<strong>Success!</strong> Ticket created.");
        this.onCreateComplete(ticket._id);

      })
    } else {
      this.msg.error("<strong>Missing!</strong> Some fields are require.");
    }
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(e) {
    this.onDescardTicket()
  }

  getMemoTypeText(memoType: MemoTypeEnum): string {
    switch (memoType) {
      case MemoTypeEnum.COMMENT:
        return "Internal Comments";
      case MemoTypeEnum.ISSUE:
        return "Request Description";
      case MemoTypeEnum.REPLY:
        return "Response";
    }
  }

  fileUpload(files: FileList) {
    this.replyMemo.files = this.replyMemo.files || [];
    this.userService.uploadFile(files.item(0)).subscribe(data => {
      this.replyMemo.files.push({
        name: data[0].name,
        id: data[0]._id
      });
    });
  }

}
