var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Roles = new Schema({
    name: {
        type: String,
        required: true
    },
    display_name: {
        type: String,
        required: true
    },
    icon: {
        type: String,
        default: "fa fa-user"
    },
    type: {
        type: String,
        default: 'USER',
        enum: ["ADMIN", "USER"]
    },
    description: {
        type: String,
        default: ''
    },
    district: {
        type: String,
        default: ''
    },
    taluk: {
        type: String,
        default: ''
    },
    actions: Schema.Types.Mixed,
    access: {
        users: {
            type: Boolean,
            default: false
        },
        settings: {
            type: Boolean,
            default: false
        },
        report: {
            type: Boolean,
            default: false
        },
        document: {
            type: Boolean,
            default: false
        },
        ticket: {
            type: Boolean,
            default: false
        }
    }
});


module.exports = mongoose.model("roles", Roles);