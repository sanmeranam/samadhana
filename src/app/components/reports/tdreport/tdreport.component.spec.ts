import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TdreportComponent } from './tdreport.component';

describe('TdreportComponent', () => {
  let component: TdreportComponent;
  let fixture: ComponentFixture<TdreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TdreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TdreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
