import { HttpClientModule } from '@angular/common/http';
import { ConfigService, ConfigModule } from './../../services/config.service';
import { UserService } from './../../services/user.service';
import { FormsModule } from '@angular/forms';
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { loginComponent } from "./login.component";

@NgModule({
    declarations: [loginComponent],
    imports: [BrowserModule, FormsModule,HttpClientModule],
    providers: [
        ConfigService,
        ConfigModule.init(),
        UserService
    ]
})

export class LoginModule { }