import { Directive, ElementRef, Input, OnInit } from '@angular/core';

declare var jQuery: any;
declare var Quill: any;
@Directive({
  selector: '[appSummernote]'
})
export class SummernoteDirective implements OnInit {

  @Input() appSummernote: any;
  @Input() key: string;
  private nativEle: any;
  constructor(el: ElementRef) {
    this.nativEle = el.nativeElement;
  }

  ngOnInit(): void {
    jQuery(this.nativEle).trumbowyg();

  }

}
