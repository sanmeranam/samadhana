var Ticket = require("../schemas/Ticket.schema");

var fnPromiseToResponse = function (oProms, res, hook) {
    oProms.then(function (data) {
        res.json(data);
        if (hook) hook(data)
    }).catch(function (e) {
        res.json(e);
    });
};


module.exports = ReportController = {
    _createQuery: function (queryObject) {
        var query = {}, v;
        Object.keys(queryObject).forEach(function (k, i, a) {
            v = queryObject[k];
            if (v.low) {
                if (v.low == "Today") {
                    v.low = new Date().toString()
                }
                if (v.low == "Yesterday") {
                    var d = new Date();
                    d.setDate(d.getDate() - 1);
                    v.low = d.toString()
                }
                if (v.high) {
                    query[v.type] = {
                        $gte: v.low,
                        $lt: v.high
                    }
                } else {
                    query[v.type] = v.low;
                }

            }
        });
        return query;
    },
    getTicketList: function (req, res) {
        var queryObject = req.body;
        var query = ReportController._createQuery(queryObject);

        fnPromiseToResponse(Ticket.find(query).then(function (data) {
            return Ticket.populate(data, [
                'history.create.user',
                'history.modified.user',
                'stage.history.user',
                'stages.current.user',
                'memo.user'
            ])
        }), res);
    },
    getUserList: function (req, res) {

    },
    downloadPDF: function (req, res) {

    },
    downloadExcel: function (req, res) {

    }
};