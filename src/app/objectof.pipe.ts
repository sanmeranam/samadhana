import { TicketModel, MemoModel } from './models/ticket.model';
import { UserModel, RoleTypeEnum } from './models/user.model';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectOf'
})
export class ObjectOfPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let keys = [];
    for (let key in value) {
      keys.push({ key: key, value: value[key] });
    }
    return keys;
  }

}


@Pipe({
  name: 'limitTo'
})
export class LimitToPipe implements PipeTransform {

  transform(value: string, args: number): any {
    if(value.length>args){
      return value.substring(0,args)+"...";
    }else{
      return value;
    }
  }

}

@Pipe({
  name: 'userSearch'
})
export class UserSearchPipe implements PipeTransform {

  transform(items: UserModel[], filter: string): any {
    if (!items || !filter) {
      return items;
    }
    return items.filter(item => item.first_name.indexOf(filter) !== -1 || item.last_name.indexOf(filter) !== -1);
  }

}

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(items: any[], filter: string): any {
    return items.sort((a: any, b: any) => {
      let ad = typeof (a[filter]) == "string" ? new Date(a[filter]) : a[filter];
      let bd = typeof (b[filter]) == "string" ? new Date(b[filter]) : b[filter];

      if (ad.getTime() > bd.getTime()) {
        return -1;
      } else if (ad.getTime() < bd.getTime()) {
        return 1;
      } else {
        return 0;
      }
    });
  }

}