import { UserModel, RoleModel } from './../models/user.model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient } from '@angular/common/http';
import { map, mergeMap, isEmpty, flatMap } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class UserService {
  private currentUser = new ReplaySubject<UserModel>(1);
  private userMeObservable: Observable<UserModel>;
  constructor(private config: ConfigService, private http: HttpClient) {
    this.userMeObservable = this.currentUser.asObservable();
    this.currentUser.next(null);
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${this.config.getApi()}/login`, { username: username, password })
      .pipe(map(user => {
        if (user && user.token) {
          localStorage.removeItem('_user');
          sessionStorage.removeItem('me');
          localStorage.setItem('_user', JSON.stringify(user));
        }
        return user;
      }));
  }

  public logout() {
    localStorage.removeItem('_user');
    sessionStorage.removeItem('me');
  }

  public hasSession() {
    return localStorage.getItem("_user");
  }

  public sessionForRoute() {
    return this.http.get<any>(`${this.config.getApi()}/me`).map((item) => {
      this.currentUser.next(item);
      return item;
    });
  }

  public me(force?: boolean, data?: any) {


    return this.currentUser.flatMap((value) => {
      if (value) {
        return this.currentUser.asObservable().map((item) => {
          item.data = data;
          return item;
        });
      } else {
        return this.http.get<any>(`${this.config.getApi()}/me`).map((item) => {
          item.data = data;
          this.currentUser.next(item);
          return item;
        });
      }
    });
  }

  public getAllUsers() {
    return this.http.get<UserModel[]>(`${this.config.getApi()}/user`);
  }


  public getAllRoles() {
    return this.http.get<RoleModel[]>(`${this.config.getApi()}/roles`);
  }

  public deleteRole(gp: RoleModel) {
    return this.http.delete<RoleModel>(`${this.config.getApi()}/roles/${gp._id}`);
  }

  public deleteUser(user: UserModel) {
    return this.http.delete<RoleModel>(`${this.config.getApi()}/user/${user._id}`);
  }

  public updateRole(gp: RoleModel) {
    return this.http.post<RoleModel>(`${this.config.getApi()}/roles/${gp._id}`, gp);
  }


  public editUser(user: UserModel) {
    user.role = typeof (user.role) === "object" ? user.role._id : user.role;
    return this.http.post(`${this.config.getApi()}/user/${user._id}`, user);
  }

  public getUserReports(filter: any) {
    return this.http.post(`${this.config.getApi()}/user_report`, filter);
  }

  public createUser(user: UserModel) {
    user.role = typeof (user.role) === "object" ? user.role._id : user.role;
    return this.http.post(`${this.config.getApi()}/user`, user);
  }

  public uploadFile(fileToUpload: File) {
    const endpoint = `${this.config.getApi()}/file`;
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http
      .post(endpoint, formData);
  }

  public getUsersByStageAllowed(stage: string, type: string, district: string, taluk: string) {
    return this.http.post<UserModel[]>(`${this.config.getApi()}/user/byStage`, { stage, type, district, taluk });
  }
}
