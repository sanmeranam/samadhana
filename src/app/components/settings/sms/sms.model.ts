import { TicketModel } from './../../../models/ticket.model';

export class SMSSettingsParam {
    constructor(public name: string, public value: string) { }
}

export class SettingModel<T>{
    _id?: string;
    name: string;
    value: T
}

export class SMSSettingsModel {
    _id?: string;
    url: string;
    method: string;
    params: SMSSettingsParam[];
    constructor() {
        this.params = [];
    }
}

export class TransactionsModel {
    _id: string;
    to: string;
    ticket?: TicketModel;
    status: string;
    date: Date;
    content: string;
    type: string;
}

export class SMSTemplateModel {
    name: string;
    type: string;
    local: boolean = false;
    content: string;
}