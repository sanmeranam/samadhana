import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { smoothlyMenu } from '../../../app.helpers';

declare var jQuery: any;

@Component({
    selector: 'navigation',
    styles: ['navigation.component.css'],
    templateUrl: 'navigation.template.html'
})

export class NavigationComponent {

    public user: any = {};

    constructor(private router: Router, private userService: UserService, private routeData: ActivatedRoute) {
        this.user = routeData.snapshot.data.message;
    }

    toggleNavigation(): void {
        jQuery("body").toggleClass("mini-navbar");
        if (jQuery("body").hasClass("mini-navbar")) {
            jQuery("#top-nav-close").hide();
        } else {
            jQuery("#top-nav-close").show();
        }
        smoothlyMenu();
    }

    logout() {
        this.userService.logout();
        this.router.navigate(['/login']);
    }

    goto(path, id?: string) {
        this.router.navigate([path]);
    }

    accessCheck(root: string, path: string) {
        if (!this.user) return false;
        return this.user.role.access[root][path];
    }

    accessSingleCheck(root: string) {
        if (!this.user) return false;
        return this.user.role.access[root];
    }

    accessInstanceChildCheck(root: string, id: string) {
        if (!this.user) return false;
        let elem = this.user.role.access[root];
        elem = elem.filter((i) => i.wf == id && i.access);
        return elem.length > 0;
    }

    accessInstanceCheck(root: string) {
        if (!this.user) return false;
        let elem = this.user.role.access[root];
        let final = false;
        elem.forEach(element => {
            final = final || element.access;
        });
        return final;
    }

    accessCheckRoot(root: string) {
        if (!this.user) return false;
        let final = false;
        for (let key in this.user.role.access[root]) {
            let value = this.user.role.access[root][key];
            final = final || value;
        }
        return final;
    }

    ngAfterViewInit() {
        jQuery('#side-menu').metisMenu();
    }

    activeRoute(routename: string): boolean {
        return this.router.url.indexOf(routename) > -1;
    }


}