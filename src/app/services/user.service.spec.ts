import { ConfigService } from './config.service';
import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClient } from 'selenium-webdriver/http';

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService]
    });
  });

  it('should be created', inject([UserService,ConfigService,HttpClient], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  it('logout method', inject([UserService,ConfigService,HttpClient], (service: UserService) => {
    localStorage.setItem('_user', "1234");
    sessionStorage.setItem('me', "qweqe");
    service.logout();
    expect(localStorage.getItem("_user")).toBeNull();
    expect(localStorage.getItem("me")).toBeNull();
  }));
});
