import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { TicketModel } from '../../models/ticket.model';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  @ViewChild('reportTab') ngbTabSet;

  opendTicketList: any[];
  stageManager: any={};

  constructor() {
    this.opendTicketList = [];
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {

  }


  onTicketNav(ticket: TicketModel) {
    let id = this.opendTicketList.push(ticket);

    setTimeout(() => {
      this.ngbTabSet.select(this.ngbTabSet.tabs.last.id);
    }, 200)

  }

  onTicketClose(index, event) {
    event.preventDefault();
    this.opendTicketList.splice(index, 1);
  }
}
