var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require("fs");
var passport = require('passport');
var busboyBodyParser = require('busboy-body-parser');

var server_services = require('./routes/server');
var global = require('./global');



var config = JSON.parse(fs.readFileSync(__dirname + "/server.config.json"));
var flowConfig = JSON.parse(fs.readFileSync(__dirname + "/flow.actions.json"));
var mongoose = require('mongoose');
mongoose.connect(config.dbUrl + config.domain);

var app = express();


app.use(busboyBodyParser({ multi: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
    next();
});
app.use(function (req, res, next) {
    req.config = config;
    req.flowConfig = flowConfig;
    next();
});



app.use(passport.initialize());
require('./controller/passport')(passport);


app.get(['/app/*', '/login'], function (request, response, next) {
    response.sendFile(__dirname + '/public/index.html');
});

app.use('/server', server_services);



// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


global.app = app;

module.exports = app;
