import { ActivatedRoute } from '@angular/router';
import { TicketService } from './../../../services/ticket.service';
import { ConfigService } from './../../../services/config.service';
import { UserService } from './../../../services/user.service';
import { TicketModel, MemoTypeEnum, FileModel } from './../../../models/ticket.model';
import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { MemoModel } from '../../../models/ticket.model';
import { UserModel } from '../../../models/user.model';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { AlertsService } from '../../../services/alerts.service';

declare var jQuery: any;
@Component({
  selector: 'app-ticketdetails',
  templateUrl: './ticketdetails.component.html',
  styleUrls: ['./ticketdetails.component.scss']
})
export class TicketdetailsComponent implements OnInit, OnDestroy, AfterViewChecked {


  ticket: TicketModel;
  isExpand: boolean = false;
  replyMemo: MemoModel;
  currentUser: UserModel;
  etaPie: string;
  EditMode: boolean;
  private subUserService;
  userList: UserModel[];
  fileList: FileModel[];
  flowConfig: any;
  status_list: any[];
  availableAction: any[];
  selectedAction: any;
  userByStageCache: any;

  constructor(private msg: AlertsService, private routeData: ActivatedRoute, private userService: UserService, private modalService: NgbModal, public config: ConfigService, private ticketService: TicketService) {
    this.ticket = routeData.snapshot.data.message;
    this.subUserService = this.userService.me().subscribe((user) => {
      this.currentUser = user;
    });
    this.userByStageCache = {};
    this.onAfterTicketUpdate();
  }

  ngOnDestroy(): void {
    this.subUserService.unsubscribe();
  }
  ngOnInit() {
    this.collectFileAttached();
    this.calculatePendingDays();
  }

  reloadTicket() {
    this.ticketService.getTicket(this.ticket._id).subscribe((t: TicketModel) => {
      this.ticket = t;
      this.onAfterTicketUpdate();
    });

  }
  ngAfterViewChecked(): void {
    jQuery("span.pie").peity("pie", {
      fill: ['#ed5565', '#d7d7d7', '#ffffff']
    })
  }

  onAfterTicketUpdate() {
    this.flowConfig = this.config.getFlow(this.ticket.request_type);
    this.status_list = Object.keys(this.flowConfig.STATUS_MAP);
    this.calculateAvailableActions();
    this.loadUserByStage();
    this.EditMode = false;
  }

  calculateAvailableActions() {
    let status = this.ticket.status;
    if (this.currentUser.role.actions[this.ticket.request_type][status]) {
      this.availableAction = this.flowConfig.STATUS_MAP[status].ACTIONS.map((action_name) => {
        return this.flowConfig.ACTIONS_DEFF[action_name];
      });
    } else {
      this.availableAction = [];
    }
  }

  canAccessCheck(sName) {
    let status = this.ticket.status;
    if (this.flowConfig && this.currentUser.role.actions[this.ticket.request_type][status]) {
      return this.flowConfig.STATUS_MAP[status][sName]
    }
    return false;
  }

  calculatePendingDays() {
    // let f_eta = typeof (this.ticket.eta_date) === "string" ? new Date(this.ticket.eta_date) : this.ticket.eta_date;
    // let create = typeof (this.ticket.history.create.date) === "string" ? new Date(this.ticket.history.create.date) : this.ticket.history.create.date;
    // let u_eta = typeof (this.ticket.eta_date) === "string" ? new Date(this.ticket.eta_date) : this.ticket.eta_date;

    // let expect = f_eta.getTime() - create.getTime();
    // // let actual = this.ticket.eta_date.getTime()- Date.now(); //- this.ticket.history.create.date.getTime();
    // let actual = Date.now() - create.getTime();//this.ticket.history.create.date.getTime();
    // this.etaPie = `${actual},${expect}`;
  }







  getMemoTypeText(memoType: MemoTypeEnum): string {
    switch (memoType) {
      case MemoTypeEnum.COMMENT:
        return "Internal Comments";
      case MemoTypeEnum.ISSUE:
        return "Request Description";
      case MemoTypeEnum.REPLY:
        return "Response";
    }
  }

  onReply() {
    if (!this.replyMemo) {
      this.replyMemo = new MemoModel();
      this.replyMemo.user = this.currentUser;
      this.replyMemo.date = new Date();
    }
    this.replyMemo.memo_type = MemoTypeEnum.REPLY;
  }

  onComment() {
    if (!this.replyMemo) {
      this.replyMemo = new MemoModel();
      this.replyMemo.user = this.currentUser;
      this.replyMemo.date = new Date();
    }
    this.replyMemo.memo_type = MemoTypeEnum.COMMENT;
  }

  fileUpload(files: FileList) {
    this.replyMemo.files = this.replyMemo.files || [];
    this.userService.uploadFile(files.item(0)).subscribe(data => {
      this.replyMemo.files.push({
        name: data[0].name,
        id: data[0]._id
      });
      this.collectFileAttached();
    });
  }

  onStageSave() {
    if (this.replyMemo && this.replyMemo.content) {
      this.ticket.memo.push(this.replyMemo);
      this.ticketService.doActionTicket(this.selectedAction.NAME, this.ticket._id, this.ticket)
        .subscribe((data: TicketModel) => {
          this.ticket.status = data.status;
          this.selectedAction = null;
          this.replyMemo = null;
          this.reloadTicket();
          this.msg.success("<strong>Success !</strong> changes updated.");
        });
    }
  }

  getDepthColor(index) {
    let colors = ["label-light", "label-danger", "label-danger", "label-warning", "label-success", "label-info", "label-primary"]
    return colors[index + 2];
  }

  onStageCancel() {
    this.selectedAction = null;
    this.replyMemo = null;
  }

  onClickEdit() {
    this.EditMode = true;
  }

  onSaveHandler() {
    if (this.replyMemo && this.ticket.memo.indexOf(this.replyMemo) == -1) {
      this.ticket.memo.push(this.replyMemo);
      this.replyMemo = null;
    }

    var oTicket = JSON.parse(JSON.stringify(this.ticket));
    if (oTicket.stages.current.user && typeof (oTicket.stages.current.user) == "object") {
      oTicket.stages.current.user = oTicket.stages.current.user._id;
    }

    this.ticketService.updateTicket(oTicket._id, oTicket).subscribe();


    this.EditMode = false;
  }

  onSelectProcessor(content) {
    this.loadUserByStage();


    this.modalService.open(content).result.then((result) => {
      this.ticket.stages.current.user = result;
      this.updateUserAsignChanged(this.ticket);
    }, (reason) => {

    });
  }

  loadUserByStage() {
    if (this.userByStageCache[this.ticket.status]) {
      return
    }
    this.userService.getUsersByStageAllowed(this.ticket.status, this.ticket.request_type, this.ticket.property_details.district, this.ticket.property_details.taluk).subscribe(data => {
      this.userByStageCache[this.ticket.status] = data;
    })
  }

  checkIfCurrentUserHasAccess() {
    if (this.userByStageCache[this.ticket.status]) {
      let item = this.userByStageCache[this.ticket.status].find((v) => {
        return v._id == this.currentUser._id
      });
      return item ? true : false;
    } else if (this.ticket.stages.current.user) {
      return this.ticket.stages.current.user._id == this.currentUser._id;
    }
    return false;
  }


  getSenceStyle(oAction) {
    switch (oAction.SENCE) {
      case "+VE":
        return "btn-outline-success";
      case "N":
        return "btn-outline-warning";
      case "-VE":
        return "btn-outline-danger";
    }
  }

  collectFileAttached() {
    this.fileList = [];
    this.ticket.memo.forEach((m) => {
      this.fileList = this.fileList.concat(m.files);
    })

    if (this.replyMemo) {
      this.fileList = this.fileList.concat(this.replyMemo.files);
    }
  }

  updateUserAsignChanged(ticket) {
    if (ticket.stages.current.user) {
      var stages = JSON.parse(JSON.stringify(ticket.stages));//Object.assign({}, ticket.stages);
      stages.current.user = ticket.stages.current.user._id;
      this.ticketService.updatePartialTicket(ticket._id, { stages }).subscribe();
    }
  }

  updateProrityAsignChanged(ticket: TicketModel) {
    this.ticketService.updatePartialTicket(ticket._id, { priority: ticket.priority }).subscribe();
  }


  onSelectDate(value, type) {
    let date = new Date(value.year, value.month, value.day);
    if (type == "FD") {
      this.ticket.eta_date = date;
    } else if (type == "UFD" && this.ticket.stages) {
      this.ticket.stages.current.eta_date = date;
    }
  }



  onActionPerformed(oAction) {
    if (!this.ticket.stages.current.user ||
      this.ticket.stages.current.user._id != this.currentUser._id) {
      this.ticket.stages.current.user = this.currentUser;
    }


    this.selectedAction = oAction;
    if (!this.replyMemo) {
      this.replyMemo = new MemoModel();
      this.replyMemo.user = this.currentUser;
      this.replyMemo.date = new Date();
    }
    switch (oAction.MEMO_ACTION) {
      case "ISSUE":
        this.replyMemo.memo_type = MemoTypeEnum.ISSUE;
        break;
      case "COMMENT":
        this.replyMemo.memo_type = MemoTypeEnum.COMMENT;
        break;
      case "REPLY":
        this.replyMemo.memo_type = MemoTypeEnum.REPLY;
        break;
    }

  }


}
