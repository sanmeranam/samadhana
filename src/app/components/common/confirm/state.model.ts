import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TemplateRef, Injectable } from '@angular/core';
export enum ALERT_TYPE {
    CONFIRM,
    SUCCESS = 'success',
    WARNING = 'warning',
    ERROR = 'danger'
}

export class AlertBox {
    start: Date;
    constructor(public type: ALERT_TYPE, public messaage: string) {
        this.start = new Date();
    }
}

@Injectable()
export class ConfirmState {
    title: string;
    message: string;
    type: ALERT_TYPE;
    tempalte: TemplateRef<any>;
    modal: NgbModalRef;
    alerts: AlertBox[] = [];
    addAlert(a: AlertBox) {
        let m = this.alerts.push(a);
        setTimeout(function () {
            let index = this.alerts.indexOf(this.a);
            if (index > -1)
                this.alerts.splice(index, 1);
        }.bind({
            alerts: this.alerts,
            a: a
        }), 3000);
    }
}