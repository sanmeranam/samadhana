import { UserModel } from './user.model';

export class RequestorModel {
    full_name: string;
    father_name: string;
    mother_name: string;
    app_no: string;
    contact: string;
    email: string;
}

export class PropertyDetailModel {
    district: string;
    taluk: string;
    hobli: string;
    village: string;
    patta: string;
}

export enum PriorityEnum {
    LOW = "LOW",
    MEDIUM = "MEDIUM",
    HIGH = "HIGH",
    VERY_HIGH = "VERY_HIGH",
    ESCALATE = "ESCALATE"
}


export class HistoryEntryModel {
    date: Date;
    user: UserModel;
    comment: string;
}

export class HistoryModel {
    create: HistoryEntryModel;
    modified: HistoryEntryModel[]
    constructor() {
        this.create = new HistoryEntryModel();
        this.modified = [];
    }
}



export enum MemoTypeEnum {
    REPLY = "REPLY",
    COMMENT = "COMMENT",
    ISSUE = "ISSUE"
}

export class FileModel {
    _id?: string;
    name: string;
    id: string;
    mime?: string;
    path?: string;
}

export class MemoModel {
    user: UserModel;
    date: Date;
    content: string;
    memo_type: MemoTypeEnum;
    files: FileModel[];    
    private __expand: boolean = false;
    constructor() {
        this.memo_type = MemoTypeEnum.COMMENT;
        this.files = [];
    }

    get isExapnd() {
        return this.__expand;
    }
    set isExapnd(v: boolean) {
        this.__expand = v;;
    }
}


export class StageEntryModel {
    user: UserModel;
    // user_group: string;
    status: string;
    action: string;
    in_date: Date;
    out_date: Date;
    eta_date: Date;
    constructor() { }
}


export class StageModel {
    current: StageEntryModel;
    history: StageEntryModel[];
    constructor() {
        this.current = new StageEntryModel();
        this.history = [];
    }
}

export enum RequestTypeEnum {
    SURVEY = "SURVEY",
    COMPLAIN = "COMPLAIN"
}

export class TicketModel {
    _id: string;
    ticket_num: string;
    subject_line: string;
    requestor: RequestorModel;
    property_details: PropertyDetailModel;
    priority: PriorityEnum;
    status: string;
    request_type: RequestTypeEnum;
    eta_date: Date;
    history: HistoryModel;
    stages: StageModel;
    memo: MemoModel[];
    toUser: boolean;
    constructor() {
        this.memo = [];
        this.priority = PriorityEnum.MEDIUM;
        this.requestor = new RequestorModel();
        this.property_details = new PropertyDetailModel();
        this.status = "NEW";
        this.stages = new StageModel();
        this.request_type = RequestTypeEnum.SURVEY;
        this.history = new HistoryModel();
        this._id = Math.floor(Math.random() * 10000000).toString(36);
        this.ticket_num = this.createTicketNum();
        this.eta_date = new Date(Date.now() + (1000 * 60 * 60 * 24 * 30));
        this.toUser=false;
    }

    setETA(num: Date) {
        this.eta_date = num;
    }

    private createTicketNum() {
        return Math.floor(Date.now() / 1000000).toString(36).toUpperCase() + (Date.now() - (Math.floor(Date.now() / 1000000) * 1000000));
    }
}