var restore = require('mongodb-restore');

restore({
    uri: 'mongodb://localhost:27017/samadhaana',
    root: __dirname + '/data',
    dropCollections:true
});