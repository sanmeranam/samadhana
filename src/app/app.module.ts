import { ConfirmState } from './components/common/confirm/state.model';
import { AlertsService } from './services/alerts.service';
import { RestService } from './services/rest.service';
import { TicketService } from './services/ticket.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { AppResolver, NewTicketResolver, OpenResolver } from './route.resolver';
import { JwtInterceptor } from './services/Jwt.Interceptor';
import { ConfigService, ConfigModule } from './services/config.service';
import { CanActivateChild } from '@angular/router';
import { AlwaysAuthGuard, AlwaysAuthChildrenGuard } from './app.access';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit, Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxPopper } from 'angular-popper';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

import { ROUTES } from "./app.routes";
import { HttpLoaderFactory } from "./services/HttpLoaderFactory";

import { AppComponent } from './app.component';
import { ObjectOfPipe, UserSearchPipe, OrderByPipe, LimitToPipe } from './objectof.pipe';

// App views
import { LoginModule } from "./views/login/login.module";
import { RegisterModule } from "./views/register/register.module";
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TimeAgoPipe} from 'time-ago-pipe';
import {NgxTrumbowygModule} from 'ngx-trumbowyg';
  
// App modules/components
import { LayoutsModule } from "./components/common/layouts/layouts.module";
import { UsersComponent } from './components/users/users.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MailboxComponent } from './components/mailbox/mailbox.component';
import { SettingsComponent } from './components/settings/settings.component';
import { Globals } from './app.global';
import { WebviewComponent } from './views/webview/webview.component';
import { ReportsComponent } from './components/reports/reports.component';
import { DatepickerDirective } from './custom/datepicker.directive';
import { InputCheckDirective } from './custom/input-check.directive';
import { GeneralComponent } from './components/settings/general/general.component';
import { SmsComponent } from './components/settings/sms/sms.component';
import { EmailComponent } from './components/settings/email/email.component';
import { DocumentComponent } from './components/document/document.component';
import { ConfirmComponent } from './components/common/confirm/confirm.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { TicketingComponent } from './components/ticketing/ticketing.component';
import { NewticketComponent } from './components/ticketing/newticket/newticket.component';
import { TicketdetailsComponent } from './components/ticketing/ticketdetails/ticketdetails.component';
import { TicketsComponent } from './components/ticketing/tickets/tickets.component';
import { RicheditorComponent } from './components/ticketing/newticket/richeditor/richeditor.component';
import { TlreportComponent } from './components/reports/tlreport/tlreport.component';
import { TdreportComponent } from './components/reports/tdreport/tdreport.component';
import { UserreportComponent } from './components/reports/userreport/userreport.component';
import { NgxBarcodeModule } from 'ngx-barcode';
import { SummernoteDirective } from './custom/summernote.directive';
import { from } from 'rxjs/observable/from';




@NgModule({
  declarations: [
    AppComponent,
    ObjectOfPipe,
    UsersComponent,
    DashboardComponent,
    MailboxComponent,
    WebviewComponent,
    SettingsComponent,
    ReportsComponent,
    NewticketComponent,
    TicketdetailsComponent,
    TicketsComponent,
    UserSearchPipe,
    RicheditorComponent,
    TimeAgoPipe,
    OrderByPipe,
    LimitToPipe,
    DatepickerDirective,
    InputCheckDirective,
    GeneralComponent,
    SmsComponent,
    EmailComponent,
    DocumentComponent,
    ConfirmComponent,
    TicketingComponent,
    TlreportComponent,
    TdreportComponent,
    UserreportComponent,
    SummernoteDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    FormsModule,
    NgxPopper,  
    LoginModule,
    RegisterModule,
    LayoutsModule,
    TagInputModule, 
    NgxBarcodeModule,
    RouterModule.forRoot(ROUTES),
    HttpClientModule,
    NgxTrumbowygModule.withConfig({
      lang: 'hu',
      svgPath: '/assets/icons.svg',
      removeformatPasted: true,
      autogrow: true,
      btns: [
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat']
      ]
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FroalaEditorModule.forRoot(), 
    FroalaViewModule.forRoot()
  ],
  exports: [
    TranslateModule,
    DatepickerDirective
  ],
  providers: [
    AlwaysAuthGuard,
    AlwaysAuthChildrenGuard,
    ConfigService,
    ConfigModule.init(),
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    AppResolver,
    OpenResolver,
    NewTicketResolver,
    TicketService,
    RestService,
    AlertsService,
    ConfirmState
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
