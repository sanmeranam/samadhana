import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Chart } from 'chart.js';
import { TicketService } from '../../services/ticket.service';

@Component({
  selector: 'app-webview',
  templateUrl: './webview.component.html',
  styleUrls: ['./webview.component.scss']
})
export class WebviewComponent implements OnInit, AfterViewInit {


  chart: any;
  searchBase: any;
  dashboard: any;

  constructor(private ticket: TicketService) {
    this.onResetSearch();
    this.dashboard = this.getDefaultData();
    this.ticket.webviewData().subscribe(data => {
      this.dashboard = this.formatData(data);
    });
  }

  ngOnInit() {
  }

  private getDefaultData() {
    return {
      MONTH: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      STATUS: {},
      TOTAL: 0,
      ACCESS: {},
      data: false
    };
  }

  private formatData(data) {
    let result = this.getDefaultData();
    data.forEach(element => {
      element.forEach(item => {
        if (item.type[0] == "MONTH") {
          result.MONTH[item._id - 1] = item.count
        }
        if (item.type[0] == "STATUS") {
          result.STATUS[item._id] = item.count
        }
        if (item.type[0] == "TOTAL") {
          result.TOTAL = item.count
        }
        if (item.type[0] == "ACCESS") {
          result.ACCESS[item._id] = item.count
        }
      });
    });
    result.data = true;
    if (this.chart) {
      this.chart.data.datasets[0].data = result.MONTH;
      this.chart.update();
    }
    return result;
  }

  getSolvedCount() {
    this.dashboard.STATUS.COMPLETED = this.dashboard.STATUS.COMPLETED || 0
    return {
      count: this.dashboard.STATUS.COMPLETED,
      per: { width: (this.dashboard.STATUS.COMPLETED / this.dashboard.TOTAL) * 100 + "%" }
    }
  }

  getCancelCount() {
    this.dashboard.STATUS.CANCELED = this.dashboard.STATUS.CANCELED || 0;
    return {
      count: this.dashboard.STATUS.CANCELED,
      per: { width: (this.dashboard.STATUS.CANCELED / this.dashboard.TOTAL) * 100 + "%" }
    }
  }

  getInProgressCount() {
    let count = this.dashboard.STATUS.NEW || 0;
    count += this.dashboard.STATUS.PENDING_SURVEY || 0;
    count += this.dashboard.STATUS.PENDING_APPROVAL || 0;
    count += this.dashboard.STATUS.PENDING_APPROVAL || 0;
    return {
      count,
      per: { width: (count / this.dashboard.TOTAL) * 100 + "%" }
    }
  }


  ngAfterViewInit(): void {
    this.chart = new Chart('canvas', {
      type: 'bar',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [
          {
            label: "Requests",
            data: this.dashboard.MONTH,
            backgroundColor: [
              'rgba(0, 99, 132, 0.6)',
              'rgba(25, 99, 132, 0.6)',
              'rgba(50, 99, 132, 0.6)',
              'rgba(75, 99, 132, 0.6)',
              'rgba(100, 99, 132, 0.6)',
              'rgba(125, 99, 132, 0.6)',
              'rgba(150, 99, 132, 0.6)',
              'rgba(175, 99, 132, 0.6)',
              'rgba(200, 99, 132, 0.6)',
              'rgba(225, 99, 132, 0.6)',
              'rgba(250, 99, 132, 0.6)',
              'rgba(255, 99, 132, 0.6)'
            ],
          }
        ]
      },
      options: {
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }

  onSearchClick() {
    if (this.searchBase.fields.ticket_num && this.searchBase.fields.mobile) {
      this.searchBase.searching = 1;
      this.ticket.ticketSearch(this.searchBase.fields).subscribe((data: any) => {
        this.searchBase.result = data.error ? null : data;
        this.searchBase.searching = 2;
      });
    }
  }

  onResetSearch(render?: boolean) {
    this.searchBase = {
      fields: {
        ticket_num: "",
        mobile: ""
      },
      searching: 0,//1-searching, 2 search finish 
      result: null
    }
    if (render) {
      setTimeout(this.ngAfterViewInit.bind(this), 500);
    }
  }
}
