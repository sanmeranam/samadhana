var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Access = new Schema({
    remote: { type: String },
    count: { type: Number },
    date: { type: Date },
    type: { type: String },
    agent: { type: String }
});



module.exports = mongoose.model("access", Access);