import { TranslateService } from '@ngx-translate/core';
import { Component } from '@angular/core';

@Component({
    selector: 'footer',
    templateUrl: 'footer.template.html'
})
export class FooterComponent {
    constructor(private trans:TranslateService){

    }

    change(to){
        this.trans.use(to)
    }
 }