import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable, APP_INITIALIZER } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';


@Injectable()
export class ConfigService {

  private _config: Object;
  private _flow: Object;
  private _env: string;

  constructor(private _http: HttpClient, private location: Location) { }

  load() {
    return new Promise((resolve, reject) => {
      this._env = 'app.config.dev';

      if (environment.production)
        this._env = 'app.config';



      this._http.get(this.location.prepareExternalUrl('./' + this._env + '.json'))
        .subscribe((data) => {
          this._config = data;

          this._http.get(`${data["backend"]["api"]}/flow`).subscribe((flow) => {
            this._flow = flow;
            resolve(true);
          })
        },
          (error: any) => {
            console.error(error);
            return Observable.throw(error || 'Server error');
          })
    });
  }
  // Is app in the development mode?
  isDevmode() {
    return this._env === 'app.config.dev';
  }
  // Gets API route based on the provided key
  getApi(): string {
    return this._config["backend"]["api"];
  }
  // Gets a value of specified property in the configuration file
  get(key: any) {
    return this._config[key];
  }
  getFlow(type?: string) {
    return type?this._flow[type]:this._flow;
  }
}

export function ConfigFactory(config: ConfigService) {
  return () => config.load();
}

export function init() {
  return {
    provide: APP_INITIALIZER,
    useFactory: ConfigFactory,
    deps: [ConfigService],
    multi: true
  }
}

const ConfigModule = {
  init: init
}

export { ConfigModule };