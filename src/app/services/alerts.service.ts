import { ConfirmState, AlertBox, ALERT_TYPE } from './../components/common/confirm/state.model';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class AlertsService {

  constructor(private modalService: NgbModal, private state: ConfirmState) { }

  confirm(message: string, title: string = 'Confirm'): Observable<boolean> {
    this.state.message = message;
    this.state.title = title;
    this.state.modal = this.modalService.open(this.state.tempalte, { size: 'sm', centered: true });
    let confirmResult = new Subject<boolean>();

    this.state.modal.result.then((res) => {
      confirmResult.next(res === 'yes');
    });
    return confirmResult.asObservable();
  }

  success(message: string) {
    this.state.addAlert(new AlertBox(ALERT_TYPE.SUCCESS, message));
  }

  error(message: string) {
    this.state.addAlert(new AlertBox(ALERT_TYPE.ERROR, message));
  }

  warnging(message: string) {
    this.state.addAlert(new AlertBox(ALERT_TYPE.WARNING, message));
  }
}
