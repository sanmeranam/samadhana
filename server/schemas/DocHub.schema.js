var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var DocHub = new Schema({
    file: { type: Schema.Types.ObjectId, ref: "file" },
    added_date: { type: Date, default: Date.now },
    access: [
        { type: Schema.Types.ObjectId, ref: "roles" },
    ]
});



module.exports = mongoose.model("dochub", DocHub);