var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var File = new Schema({
    name: { type: String },
    path: { type: String },
    mime: { type: String }
});



module.exports = mongoose.model("file", File);