import { UserreportComponent } from './components/reports/userreport/userreport.component';
import { TicketingComponent } from './components/ticketing/ticketing.component';
import { DocumentComponent } from './components/document/document.component';
import { WebviewComponent } from './views/webview/webview.component';
import { AppResolver, OpenResolver } from './route.resolver';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersComponent } from './components/users/users.component';
import { AlwaysAuthChildrenGuard, AlwaysAuthGuard } from './app.access';
import { Routes } from "@angular/router";
import { loginComponent } from "./views/login/login.component";
import { blankComponent } from "./components/common/layouts/blank.component";
import { basicComponent } from "./components/common/layouts/basic.component";
import { SettingsComponent } from './components/settings/settings.component';
import { ReportsComponent } from './components/reports/reports.component';
import { TicketsComponent } from './components/ticketing/tickets/tickets.component';
import { NewticketComponent } from './components/ticketing/newticket/newticket.component';
import { TicketdetailsComponent } from './components/ticketing/ticketdetails/ticketdetails.component';
import { TlreportComponent } from './components/reports/tlreport/tlreport.component';
import { TdreportComponent } from './components/reports/tdreport/tdreport.component';


export const ROUTES: Routes = [
  // App views
  {
    path: 'app',
    component: basicComponent,
    canActivate: [AlwaysAuthGuard],
    canActivateChild: [AlwaysAuthChildrenGuard],
    resolve: { message: AppResolver },
    children: [
      { path: '', redirectTo: 'tickets', pathMatch: 'full' },
      {
        path: 'home', component: DashboardComponent
      },
      {
        path: 'tickets', component: TicketingComponent,
        children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'list', component: TicketsComponent },
          {
            path: 'new',
            component: NewticketComponent
          },
          { path: 'details/:id', component: TicketdetailsComponent, resolve: { message: OpenResolver } }
        ]
      },
      { path: 'documents', component: DocumentComponent },
      {
        path: 'settings',
        component: SettingsComponent
      },
      {
        path: 'reports',
        component: ReportsComponent,
        children: [
          { path: '', redirectTo: 'tl', pathMatch: 'full' },
          { path: 'tl', component: TlreportComponent },
          { path: 'td', component: TdreportComponent },
          { path: 'user', component: UserreportComponent }
        ]
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path: 'profile/:id',
        component: UsersComponent
      }
    ]
  },
  {
    path: '', component: blankComponent,
    children: [
      { path: '', component: WebviewComponent },
      { path: 'login', component: loginComponent }
    ]
  },

  // Handle all other routes
  { path: '**', component: WebviewComponent }
];
