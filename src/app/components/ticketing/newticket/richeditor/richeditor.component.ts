import { MemoModel } from './../../../../models/ticket.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-richeditor',
  templateUrl: './richeditor.component.html',
  styleUrls: ['./richeditor.component.scss']
})
export class RicheditorComponent implements OnInit {

  @Input() content: MemoModel;
  @Input() mode: string;
  @Input() name: string;
  editorConfig: any;

  constructor() {
    this.mode = this.mode || 'DISPLAY';
  }

  ngOnInit() {
  }

}
