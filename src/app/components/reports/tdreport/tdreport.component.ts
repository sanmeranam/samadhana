import { TicketModel } from './../../../models/ticket.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tdreport',
  templateUrl: './tdreport.component.html',
  styleUrls: ['./tdreport.component.scss']
})
export class TdreportComponent implements OnInit {

  @Input() ticket:TicketModel;

  constructor() { }

  ngOnInit() {
  }

  onPagePrint(){
    window.print();
  }

}
