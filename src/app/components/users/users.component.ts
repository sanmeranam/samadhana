import { ConfirmComponent } from './../common/confirm/confirm.component';
import { ActivatedRouteSnapshot } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { RestService, TABLE_TYPE } from './../../services/rest.service';
import { ConfigService } from './../../services/config.service';
import { UserService } from './../../services/user.service';
import { UserModel, RoleModel, RoleTypeEnum } from './../../models/user.model';
import { Component, OnInit } from '@angular/core';
import { AlertsService } from '../../services/alerts.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ["users.component.scss"]
})
export class UsersComponent implements OnInit {

  userList: UserModel[] = [];

  groupedList: any;
  currentGroup: string;
  roleList: RoleModel[] = [];
  roleKeyMap = {};
  userLoaded: boolean = false;
  userLoading: boolean = false;
  rolesLoaded: boolean = false;
  showOnlyUser: string;
  fieldMandatError: any;
  userSearchText: string = '';
  totalUsers: number = 0;
  avtarList: string[];
  newRole: RoleModel;
  newRoleSavingStatus: string;
  public distctMap: any[];
  public talukMap: any[];
  selectedDist: any;

  selectedUser = {
    user: null,
    mode: 0,//0-display,1-edit,2-new
    changed: false,
    message: '',
    clone: null
  };
  selectedTaluk: any;

  constructor(private msg: AlertsService, private userService: UserService, private config: ConfigService, private restService: RestService, routeData: ActivatedRoute) {
    this.showOnlyUser = routeData.snapshot.params.id;

    this.groupedList = {
      ALL: { display_name: 'All', icon: 'fa fa-users', list: [], displayOnly: true },
      INACTIVE: { display_name: 'Inactive', icon: 'fa fa-users', list: [], displayOnly: true }
    };
    this.currentGroup = "ALL";
    if (this.showOnlyUser) {
      this.userService.me().subscribe((user: any) => {
        this.onSelectUser(JSON.parse(JSON.stringify(user)));
      });
    }

    restService.getByField(TABLE_TYPE.SETTINGS, 'name', 'city').subscribe((district: any[]) => {
      if (district.length) {
        this.distctMap = district[0].value;
      }
    });
  }

  ngOnInit() {
    this.loadUsers();
    this.loadRoles();
    this.fieldMandatError = {
      f_name: false,
      l_name: false,
      role: false,
      username: false,
      password: false
    };

    for (let i = 0; i < 100; i++) {
      this.userList.push(new UserModel());
    }

    this.avtarList = [
      "https://www.w3schools.com/bootstrap4/img_avatar3.png",
      "https://www.w3schools.com/bootstrap4/img_avatar2.png",
      "https://www.w3schools.com/bootstrap4/img_avatar4.png",
      "https://www.w3schools.com/bootstrap4/img_avatar6.png"
    ];
  }

  onDistrictSelect() {
    if (this.newRole.district) {
      this.selectedDist = this.distctMap.filter(item => item.name === this.newRole.district)[0];
    }
  }

  onTalukSelect() {
    if (this.newRole.taluk) {
      this.selectedTaluk = this.selectedDist.city.filter(item => item.name === this.newRole.taluk)[0];
    }
  }

  fileUpload(files: FileList) {
    this.userService.uploadFile(files.item(0)).subscribe(data => {
      this.selectedUser.user.avtar = this.config.getApi() + '/file/' + data[0]._id
    });
  }

  getGroupNames() {
    return Object.keys(this.groupedList);
  }

  onSelectUser(user: UserModel) {
    this.selectedUser.user = user;
    this.selectedUser.mode = 0;
    this.selectedUser.clone = this.clone(user);
  }

  onDeleteGroup(oGroup) {
    this.msg.confirm("Are you sure, want to delete this group?", "Warrning!")
      .subscribe((value) => {
        if (value) {
          this.userService.deleteRole(oGroup).subscribe(() => {
            this.msg.success("<strong>Success!</strong> User group deleted.")
          });
        }
      })
  }

  onCreateNewUser() {
    this.selectedUser.user = new UserModel();
    this.selectedUser.mode = 2;
    this.fieldMandatError = {
      f_name: true,
      l_name: true,
      role: true,
      username: true,
      password: true
    };
  }

  onSaveHandler() {
    if (this.selectedUser.user && this.selectedUser.mode == 1) {
      this.updateUserSave(this.selectedUser.user);
    } else if (this.selectedUser.user && this.selectedUser.mode == 2) {
      this.newUserSave(this.selectedUser.user);
    }
  }

  onCancelHandler() {
    if (this.selectedUser.mode === 1) {
      this.assign(this.selectedUser.user, this.selectedUser.clone);
      this.selectedUser.mode = 0;
      this.selectedUser.changed = false;
    } else {
      this.selectedUser.mode = 0;
      this.selectedUser.changed = false;
      this.selectedUser.user = null;
      this.selectedUser.clone = null;
      this.selectedUser.message = null;
    }
  }

  newUserSave(user: UserModel) {
    if (this.validateFields(user, true))
      this.userService.createUser(user).subscribe((data: UserModel | any) => {
        if (data._id) {
          this.selectedUser.mode = 0
          this.selectedUser.user._id = data._id;
          this.selectedUser.changed = false;
          this.msg.success('<strong>Success!</strong> User created.');
          this.loadUsers();
        } else {
          this.msg.error('<strong>Failed!</strong> User creation.');
        }
      });

  }
  updateUserSave(user: UserModel) {
    if (this.validateFields(user, false))
      this.userService.editUser(user).subscribe((data: UserModel | any) => {
        if (data._id) {
          this.selectedUser.mode = 0
          this.selectedUser.changed = false;
          this.msg.success('<strong>Success!</strong> User details updated.');
        } else {
          this.msg.error("<strong>Failed!</strong> User details update.");
        }
      });
  }

  validateFields(user: UserModel, isNew: boolean) {
    this.fieldMandatError.f_name = user.first_name ? true : false;
    this.fieldMandatError.l_name = user.last_name ? true : false;
    this.fieldMandatError.username = user.username ? true : false;
    if (isNew) {
      this.fieldMandatError.password = user.password ? true : false;
    } else {
      this.fieldMandatError.password = true;
    }
    this.fieldMandatError.role = user.role ? true : false;
    return this.fieldMandatError.f_name &&
      this.fieldMandatError.l_name &&
      this.fieldMandatError.username &&
      this.fieldMandatError.password &&
      this.fieldMandatError.role
  }

  loadUsers() {
    this.userLoaded = false;
    this.userLoading = true;
    this.userService.getAllUsers().subscribe((list: UserModel[]) => {
      this.userList = list;
      this.userLoaded = true;
      this.userLoading = false;
      if (this.rolesLoaded) {
        this.applyGroupFilter();
      }
    })
  }

  applyGroupFilter() {
    this.groupedList.ALL.list = this.userList;
    this.groupedList.INACTIVE.list = this.userList.filter(u => !u.active);
    this.userList.forEach(user => this.groupedList[this.roleKeyMap[user.role].name].list = []);
    this.userList.forEach(user => this.groupedList[this.roleKeyMap[user.role].name].list.push(user));
  }

  appplyUserChange(name?: string, value?: any) {
    if (name && value != undefined) {
      this.selectedUser.user[name] = value;
    }
    this.selectedUser.mode = 1;
    this.selectedUser.changed = true;
    this.validateFields(this.selectedUser.user, false);
  }

  onUserDelete() {
    this.msg.confirm("Are you sure want to delete this user?", "Warrning !")
      .subscribe((value) => {
        if (value) {
          this.userService.deleteUser(this.selectedUser.user).subscribe(() => {
            this.msg.success('<strong>Success!</strong> User account deleted.');
            this.selectedUser = {
              user: null,
              mode: 0,//0-display,1-edit,2-new
              changed: false,
              message: '',
              clone: null
            };
            this.loadUsers();
          });
        }
      });
  }

  createNewRole() {
    this.newRoleSavingStatus = null;
    this.newRole = new RoleModel();
    this.newRole.type = RoleTypeEnum.USER;
    this.onChangeUserType(this.newRole);
  }

  onChangeUserType(newRole) {
    this.newRoleSavingStatus = null;
    if (newRole && newRole.type == RoleTypeEnum.ADMIN) {
      newRole.access = {
        users: true,
        settings: true,
        ticket: true,
        report: true,
        document: true,
      };

      newRole.actions = {
        SURVEY: {
          NEW: true,
          PENDING_SURVEY: true,
          PENDING_APPROVAL: true,
          APPROVED: true,
          ON_HOLD: true,
          INCORRECT_INFO: true,
          COMPLETED: true,
          CANCELED: true
        }
      };
    } else {
      newRole.access = {
        users: false,
        settings: false,
        ticket: true,
        report: false,
        document: true
      };

      newRole.actions = {
        SURVEY: {
          NEW: true,
          PENDING_SURVEY: false,
          PENDING_APPROVAL: false,
          APPROVED: false,
          ON_HOLD: true,
          INCORRECT_INFO: true,
          COMPLETED: false,
          CANCELED: true
        }
      };
    }
  }

  onTicketAccessChanged(newRole) {
    this.newRoleSavingStatus = null;
    if (newRole && !newRole.access.ticket) {
      newRole.actions = {
        SURVEY: {
          NEW: true,
          PENDING_SURVEY: false,
          PENDING_APPROVAL: false,
          APPROVED: false,
          ON_HOLD: true,
          INCORRECT_INFO: true,
          COMPLETED: false,
          CANCELED: true
        }
      };
    }
  }

  saveNewRole(popContentUserGroup: any) {
    if (this.newRole.type == RoleTypeEnum.USER && (!this.newRole.district || !this.newRole.taluk)) {
      this.newRoleSavingStatus = "<strong>Error!</strong> District and taluk is require for user type group.";
      return;
    }

    if (this.newRole.display_name && this.newRole.name) {
      this.newRoleSavingStatus = "<strong><i class='fa fa-spinner fa-spin'></i></strong></strong> Creating new role..";

      this.restService.create<RoleModel>(TABLE_TYPE.ROLE, this.newRole).subscribe((data) => {
        popContentUserGroup.close();
        this.loadRoles();
      });
    } else {
      this.newRoleSavingStatus = "<strong>Error!</strong> Blank fields are not allowed.";
    }

  }

  saveEditRole(popContentUserGroup: any, oGroup: RoleModel) {
    if (oGroup.type == RoleTypeEnum.USER && (!oGroup.district || !oGroup.taluk)) {
      this.msg.error("<strong>Error!</strong> District and taluk is require for user type group.");
      return;
    }


    let gp: any = Object.assign({}, oGroup);
    delete (gp.list)
    delete (gp.icon)
    this.userService.updateRole(gp).subscribe(() => {
      this.msg.success("<strong>Success!</strong> User group updated.");
    });
    popContentUserGroup.close();
  }

  loadRoles() {
    this.rolesLoaded = false;
    this.userService.getAllRoles().subscribe((rl: RoleModel[]) => {
      this.roleList = rl;
      rl.forEach((item) => {
        this.roleKeyMap[item._id] = item;
        this.groupedList[item.name] = item;
        this.groupedList[item.name].list = [];
      })
      this.rolesLoaded = true;
      if (this.userLoaded) {
        this.applyGroupFilter();
      }
    });
  }

  private clone(instance): UserModel {
    let copy = new UserModel()
    copy = Object.assign(copy, instance);
    return copy;
  }

  assign(to: UserModel, from: UserModel) {
    return Object.assign(to, from);
  }


  getDistrictByRole(id: string) {
    return this.roleList.filter(i => i._id == id)[0].district;
  }
  getTalukByRole(id: string) {
    return this.roleList.filter(i => i._id == id)[0].district;
  }
}