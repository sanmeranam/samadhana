var FileModel = require("../schemas/File.schema");
var DocHub = require("../schemas/DocHub.schema");
var mongoose = require('mongoose');
var fs = require('fs');

module.exports = {
    fileUpload: function (req, res) {
        if (!req.files)
            return res.status(400).send('No files were uploaded.');

        var keys = Object.keys(req.files);
        var filesUploaded = [];
        var fileSaveRequest = [];
        if (keys.length) {
            for (var i = 0; i < keys.length; i++) {
                var entry = req.files[keys[i]][0];
                var wstream = fs.createWriteStream(req.config.upload_path + '/' + entry.name);
                wstream.write(entry.data);
                wstream.end();
                fileSaveRequest.push(new FileModel({
                    name: entry.name,
                    path: req.config.upload_path + '/' + entry.name,
                    mime: entry.mimetype,
                }).save());
            }
        }

        Promise.all(fileSaveRequest).then(function (values) {
            res.json(values);
        });

    },
    fileDownload: function (req, res) {
        FileModel.findById(req.params.id).then(function (file) {
            res.download(file.path);
        });
    },
    createDocument(req, res) {
        var oDoc = req.body;
        oDoc.file = mongoose.Types.ObjectId(oDoc.file);
        oDoc.access = oDoc.access.map(function (item) {
            return mongoose.Types.ObjectId(item);
        });

        new DocHub(oDoc).save()
            .then(function (success) {
                res.json(success);
            })
            .catch(function (error) {
                res.json(error);
            });

    },
    getDocumentsByRole(req, res) {
        var roleId = req.params.roleId;
        loadDocuemnts(res, { access: mongoose.Types.ObjectId(roleId) });
    },
    getAllDocuments(req, res) {
        loadDocuemnts(res, {});
    },
    deleteDocument(req, res) {
        var doId = req.params.id;
        DocHub.findById(doId).then(function (docs) {
            return DocHub.populate(docs, [
                'file'
            ])
        }).then(function (oDoc) {
            Promise.all([
                FileModel.findByIdAndRemove(oDoc.file._id),
                DocHub.findByIdAndRemove(doId)
            ]).then(function (values) {
                fs.unlinkSync(oDoc.file.path);
                res.json({ ok: 1, error: 0 });
            });
        }).catch(function (error) {
            res.json({ ok: 0, error: 1 });
        });
    }
}

var loadDocuemnts = function (res, payload) {
    DocHub.find(payload).then(function (docs) {
        return DocHub.populate(docs, [
            'file',
            'access.roles'
        ])
    }).then(function (data) {
        res.json(data);
    }).catch(function (error) {
        res.json(error);
    });
}