var User = require("../schemas/User.schema");
var authConfig = require("./auth.config");
let passport = require('passport');
let jwt = require('jsonwebtoken');

module.exports = {
    login: function (req, res) {
        User.findOne({
            username: req.body.username
        }, function (err, user) {
            if (err) throw err;

            if (!user) {
                res.send({
                    success: false,
                    message: 'Authentication failed. User not found.'
                });
            } else if (!user.active) {
                res.send({
                    success: false,
                    message: 'Account deactivated. Please contact administrator.'
                });
            } else {
                // Check if password matches
                user.comparePassword(req.body.password, function (err, isMatch) {
                    if (isMatch && !err) {

                        // Create token if the password matched and no error was thrown
                        var token = jwt.sign(user.toJSON(), authConfig.secret, {
                            expiresIn: "2 days"
                        });
                        res.json({
                            success: true,
                            message: 'Authentication successfull',
                            token
                        });
                    } else {
                        res.send({
                            success: false,
                            message: 'Authentication failed. Passwords did not match.'
                        });
                    }
                });
            }
        });
    },
    logout: function (req, res) {

    },
    me: function (req, res) {
        res.json(req.user);
    },
    checkSession: passport.authenticate('jwt', {
        session: false
    })
}