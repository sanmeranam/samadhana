import { Directive, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appInputCheck]'
})
export class InputCheckDirective {
  @Input("appInputCheck") appInputCheck: string;
  constructor() { }

  @HostListener('keypress', ['$event'])
  beforeunloadHandler(event) {
    const pattern = new RegExp(this.appInputCheck);
    let inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      event.preventDefault();
    }
  }
}
