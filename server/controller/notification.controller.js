var Settings = require("../schemas/Settings.schema");
var CommTransact = require("../schemas/Communication.schema");
var EmailController = require("./email.controller");
var SMSController = require("./sms.controller");
var _ = require("lodash");

var mobileRegex = /^[789]\d{9}$/;
var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
module.exports = Notif = {
    _sms_templates: {},
    _email_templates: {},
    _init: function () {
        Settings.findOne({ name: "sms_template" })
            .then(function (result) {
                var tmp;
                for (var i in result.value) {
                    tmp = result.value[i];
                    Notif._sms_templates[tmp.type] = tmp;
                }
            });
        Settings.findOne({ name: "mail_template" })
            .then(function (result) {
                var tmp;
                for (var i in result.value) {
                    tmp = result.value[i];
                    Notif._email_templates[tmp.type] = tmp;
                }
            });
    },
    dispatchCreationAlert: function (oTicket) {
        if (oTicket.requestor.contact && mobileRegex.test(oTicket.requestor.contact)) {
            Notif._processSMSDispach(oTicket, "INIT");
        }
        if (oTicket.requestor.email && emailRegex.test(oTicket.requestor.email.toLowerCase())) {
            Notif._processEmailDispach(oTicket, "INIT");
        }
    },
    dispatchCompleteAlert: function (oTicket) {
        if (oTicket.requestor.contact && mobileRegex.test(oTicket.requestor.contact)) {
            Notif._processSMSDispach(oTicket, "COMPLETE");
        }
        if (oTicket.requestor.email && emailRegex.test(oTicket.requestor.email.toLowerCase())) {
            Notif._processEmailDispach(oTicket, "COMPLETE");
        }
    },
    dispatchOnDemandAlert: function (oTicket) {
        if (oTicket.requestor.contact && mobileRegex.test(oTicket.requestor.contact)) {
            Notif._processSMSDispach(oTicket, "DEMAND");
        }
        if (oTicket.requestor.email && emailRegex.test(oTicket.requestor.email.toLowerCase())) {
            Notif._processEmailDispach(oTicket, "DEMAND");
        }
    },
    dispatchUserDetailsUpdate(oUser) {
        if (oUser.contact && mobileRegex.test(oUser.contact)) {
            Notif._processSMSDispach(oUser, "USER");
        }
        if (oUser.email && emailRegex.test(oUser.email.toLowerCase())) {
            Notif._processEmailDispach(oUser, "USER");
        }
    },
    _processSMSDispach: function (oContext, type) {
        if (!Notif._sms_templates[type])
            return;
        var oTemplate = Notif._sms_templates[type];
        var content = Notif._resolveTemplate(oTemplate.content, oContext);
        var msdn = oContext.requestor ? oContext.requestor.contact : oContext.contact;

        SMSController.sendSMS('91' + msdn, content, function (response) {
            new CommTransact({
                type: "SMS",
                to: msdn,
                ticket: oContext._id,
                status: oContext.status,
                content: content,
                response: response
            }).save();
        }, oTemplate.local);
    },
    _processEmailDispach: function (oContext, type) {
        if (!Notif._email_templates[type])
            return;
        var oTemplate = Notif._email_templates[type];
        var content = Notif._resolveTemplate(oTemplate.content, oContext);
        var subject = Notif._resolveTemplate(oTemplate.subject, oContext);
        var email = oContext.requestor?oContext.requestor.email:oContext.email;
        EmailController.sendEmail(email, subject, content, function (response) {
            new CommTransact({
                type: "EMAIL",
                to: email,
                ticket: oContext._id,
                status: oContext.status,
                content: content,
                response: response
            }).save();
        })
    },
    _resolveTemplate: function (content, oTicket) {
        var regs = /{(.*?)}/g;
        var aVars = content.match(regs);
        var item, key, value;
        for (var i = 0; i < aVars.length; i++) {
            item = aVars[i];
            key = item.replace("{", "").replace("}", "");
            value = _.get(oTicket, key);
            content = content.replace(new RegExp(item, "g"), value);
        }
        return content;
    }
}

Notif._init();