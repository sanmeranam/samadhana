import { TransactionsModel, SettingModel } from './../sms/sms.model';
import { AlertsService } from './../../../services/alerts.service';
import { Component, OnInit } from '@angular/core';
import { RestService, TABLE_TYPE } from '../../../services/rest.service';
import { EmailTemaplateModel, EmailConfig } from './email.model';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {

  emailTransactions: TransactionsModel[];
  emailTemplates: SettingModel<EmailTemaplateModel[]>;
  emailConfig: SettingModel<EmailConfig>;
  activeTemplate: EmailTemaplateModel;

  constructor(private rest: RestService, private alert: AlertsService) {
    this.rest.getByField(TABLE_TYPE.SETTINGS, "name", "mail_template")
      .subscribe((result: SettingModel<EmailTemaplateModel[]>[]) => {
        if (result && result.length) {
          this.emailTemplates = result[0];
          this.activeTemplate = this.emailTemplates.value[0];
        }
      });

    this.rest.getByField(TABLE_TYPE.SETTINGS, "name", "mail_config")
      .subscribe((result: SettingModel<EmailConfig>[]) => {
        if (result && result.length) {
          this.emailConfig = result[0];
        }
      });

    this.rest.getByField(TABLE_TYPE.COMM, "type", "EMAIL")
      .subscribe((result: TransactionsModel[]) => {
        this.emailTransactions = result;
      });
  }

  ngOnInit() {

  }

  onSaveEmailConfig() {
    this.rest.updateById(TABLE_TYPE.SETTINGS, this.emailConfig._id, this.emailConfig).subscribe(() => {
      this.alert.success("<strong>Success !</strong> email configurations updated.");
    });
  }

  onChangeActiveTemplate(temp) {
    this.activeTemplate = temp;
  }

  onTemplateSave() {
    this.rest.updateById(TABLE_TYPE.SETTINGS, this.emailTemplates._id, this.emailTemplates).subscribe(() => {
      this.alert.success("<strong>Success !</strong> templates updated.");
    });
  }
}
